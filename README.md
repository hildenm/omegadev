# OmegaDev

A toolchain for cross compiling and debugging C/C++ and assembler targeting Atari ST.

## Description
OmegaDev v1.0 have the following goals:

* GCC cross platform toolchain.
* Full standard C/C++ library support.
* Full symbolic remote debugging.
* Compiled binaries is compatible with unmodified Atari ST's running tos 1.02 and above.
* Full integration with VsCode.
* Linux, Windows and Mac support.

Added to this, the project also includes an optional hardware solution that have the following features:

* SD-card hard drive emulation.
* Emulated hard drives, even TOS formated, can be connected to PC as USB drives for easy file transfers.
* RS232 to USB.

## Current beta version
All features exists and all goals have been met, but much testing is required to take the project beyond the beta state.  
To highlight a few known issues:

#### Toolchain

* Standard C++ libraries is currently not working, and is most likely not useful when working due to the very large binaries they create.
* Mac is not yet supported (the developer have no mac), but Linux and Windows work well.
* There is only support for m68000 without FPU. More will be added when v1.0 reaches stable state.

#### Hardware

* Current hardware do not work on Mega STE. A fix has been found and schematics, pcb and firmware will be updated soon.
* Current PCB is four layers and will be remade into two layers to make it cheaper to produce.
* Sharing SD-card drives between ST and PC is a bit hazardous... There is no check that data isn't written at the same time from both sources. Take great care, and keep backups of shared drives.


## Getting Started

[The wiki describes everything you need to know.](https://bitbucket.org/hildenm/omegadev/wiki/)

## License

This project is licensed under the MIT License (MIT) - see the [LICENSE.md](LICENSE.md) file for details

