# OmegaDev hardware
The folder "omegadev/hardware" contains design files and firmware source for the OmegaDev hardware.  
See the [wiki](https://bitbucket.org/hildenm/omegadev/wiki/) for information about how to build.  

## Directory contents

### kicad/  
KiCad project with schematics and PCB design.

### firmware/
Raspberry Pi Pico sources for the hardware.

## License

This project is licensed under the MIT License (MIT) - see the [LICENSE.md](https://bitbucket.org/hildenm/omegadev/src/master/LICENSE.md) file for details

