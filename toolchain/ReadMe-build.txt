Build for linux:
	1. Install a complete gcc toolchain and other necessary dependencies.
	For debian systems (raspberry pi, ubuntu etc.):
		sudo apt update
		sudo apt install build-essential texinfo flex bison libgmp-dev libmpfr-dev libmpc-dev gdb-multiarch
	2. Run sudo ./build-toolchain.sh
	3. The m68k-elf toolchain with Atari ST additions will now be at /usr/local/bin/m68k-elf

Build for windows:
	1. Install msys and mingw by following instructions on https://www.msys2.org/
	2. From start menu, start "MSYS2 UCRT64".
	3. Run:
		pacman -S --needed base-devel mingw-w64-ucrt-x86_64-toolchain cpio gmp-devel mpfr-devel mpc-devel git mingw-w64-ucrt-x86_64-gdb-multiarch
	4. Run build-toolchain.sh
	5. The m68k-elf toolchain with Atari ST additions will now be at /usr/local/bin/m68k-elf (C:\msys64\usr\local\bin\m68k-elf)
