#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <m68k-elf/include/elf.h>

#define bool short
#define true 1
#define false 0


unsigned short ReadUInt16_r(FILE *fs, bool* status)
{
	size_t cnt = 0;
	unsigned short v;
	unsigned char* vp = (unsigned char*)&v;
	cnt += fread(vp + 1, 1,1, fs);
	cnt += fread(vp, 1,1, fs);
	*status &= cnt == 2;
	return v;
}

unsigned int ReadUInt32_r(FILE *fs, bool* status)
{
	size_t cnt = 0;
	unsigned int v;
	unsigned char* vp = (unsigned char*)&v;
	cnt += fread(vp + 3, 1,1, fs);
	cnt += fread(vp + 2, 1,1, fs);
	cnt += fread(vp + 1, 1,1, fs);
	cnt += fread(vp, 1,1, fs);
	*status &= cnt == 4;
	return v;
}

// Don't forget to free.
void* ReadSectionData(FILE *fs, Elf32_Shdr* sectionHeaders, size_t sectionIndex)
{
	size_t offset = sectionHeaders[sectionIndex].sh_offset;
	size_t size = sectionHeaders[sectionIndex].sh_size;
	//printf("%d, %d, %d\n",offset, size, sectionIndex);
	fseek(fs, offset, SEEK_SET);
	void* data = NULL;
	if ((data = malloc(size)) != NULL)
	{
		if (fread(data, 1, size, fs) != size)
		{
			free(data);
			data = NULL;
		}
	}
	return data;
}

bool ReadHeader(FILE* fs, Elf32_Ehdr* header)
{
	bool status = true;
	do
	{
		status &= EI_NIDENT == fread(header->e_ident, 1, EI_NIDENT, fs);
		if (!status) {break;}
		char m68kelf_magic[EI_NIDENT] = { 0x7f, 'E', 'L', 'F', 0x01, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		status &= 0 == memcmp(header->e_ident, m68kelf_magic, EI_NIDENT);
		if (!status) {break;}
		header->e_type = ReadUInt16_r(fs, &status);
		header->e_machine = ReadUInt16_r(fs, &status);
		header->e_version = ReadUInt32_r(fs, &status);
		header->e_entry = ReadUInt32_r(fs, &status);
		header->e_phoff = ReadUInt32_r(fs, &status);
		header->e_shoff = ReadUInt32_r(fs, &status);
		header->e_flags = ReadUInt32_r(fs, &status);
		header->e_ehsize = ReadUInt16_r(fs, &status);
		header->e_phentsize = ReadUInt16_r(fs, &status);
		header->e_phnum = ReadUInt16_r(fs, &status);
		header->e_shentsize = ReadUInt16_r(fs, &status);
		header->e_shnum = ReadUInt16_r(fs, &status);
		header->e_shstrndx = ReadUInt16_r(fs, &status);
		if (!status) {break;}
		status &= header->e_version == 1 && header->e_machine == 4;
		status &= header->e_ehsize == 0x34 && (header->e_phentsize == 0x20 || header->e_phentsize == 0) && header->e_shentsize == 0x28;
	} while(false);
	return status;
}

		
Elf32_Shdr* ReadSectionHeaders(FILE* fs, Elf32_Ehdr* header)
{
	Elf32_Shdr* sectionHeaders = malloc(header->e_shnum * sizeof(Elf32_Shdr));
	fseek(fs, header->e_shoff, SEEK_SET);
	for (size_t i = 0; i < header->e_shnum; ++i)
	{
		bool status = true;
		Elf32_Shdr* sh = sectionHeaders + i;
		sh->sh_name = ReadUInt32_r(fs, &status);
		sh->sh_type = ReadUInt32_r(fs, &status);
		sh->sh_flags = ReadUInt32_r(fs, &status);
		sh->sh_addr = ReadUInt32_r(fs, &status);
		sh->sh_offset = ReadUInt32_r(fs, &status);
		sh->sh_size = ReadUInt32_r(fs, &status);
		sh->sh_link = ReadUInt32_r(fs, &status);
		sh->sh_info = ReadUInt32_r(fs, &status);
		sh->sh_addralign = ReadUInt32_r(fs, &status);
		sh->sh_entsize = ReadUInt32_r(fs, &status);
		if (!status) 
		{
			free(sectionHeaders);
			sectionHeaders = NULL;
			break;
		}
	}
	return sectionHeaders;
}

int FindNamedSection(Elf32_Ehdr* header, Elf32_Shdr* sectionHeaders, const char* elfStrings, const char* name)
{
	for (int i = 0; i < header->e_shnum; ++i)
	{
		const char* sectionName = elfStrings + sectionHeaders[i].sh_name;
		if (strcmp(name, sectionName) == 0)
		{
			return i;
		}
	}	
	return -1;
}
		
bool OutputSectionProgbits(FILE* fs, FILE* fd, Elf32_Shdr* sectionHeader)
{
	bool status = false;
	void* data = NULL;
	// Section .text and .data contains progbits that we want.
	if (sectionHeader->sh_type == SHT_PROGBITS)
	{
		if ((data = malloc(sectionHeader->sh_size)) != NULL)
		{
			fseek(fs, sectionHeader->sh_offset, SEEK_SET);
			if (fread(data, 1, sectionHeader->sh_size, fs) == sectionHeader->sh_size)
			{
				if (fwrite(data, 1, sectionHeader->sh_size, fd) == sectionHeader->sh_size)
				{
					// success
					status = true;
				}
			}
		}
	}
	if (data != NULL)
	{
		free(data);
		data = NULL;
	}
	return status;
}

// Just makes sure that the alignment of the section to be written, matches the position in the written file.
bool AlignToSectionAddress(FILE* fd, Elf32_Shdr* sectionHeader)
{
	int align = sectionHeader->sh_addr - (ftell(fd) - 0x1c);
	if (align < 0)
	{
		// Cannot align backwards!
		return false;
	}
	for (;align != 0; --align)
	{
		char zero = 0;
		fwrite(&zero, 1, 1, fd);
	}
	return true;
}

bool LoadRelocs(FILE* fs, Elf32_Shdr* sectionHeader, Elf32_Rela** pt_relocs, size_t* relocnum)
{
	bool status = true;
	// Section .rela.text and .rela.data contains relocation data that we want.
	if (sectionHeader->sh_type == SHT_RELA)
	{
		fseek(fs, sectionHeader->sh_offset, SEEK_SET);
		
		size_t addnumbytes = sectionHeader->sh_size;
		size_t addnumrelocs = addnumbytes / sizeof(Elf32_Rela);
		size_t lastrelocnum = *relocnum;
		*relocnum += addnumrelocs;
		size_t totbytes = *relocnum * sizeof(Elf32_Rela);

		Elf32_Rela* relocs = *pt_relocs;
		if (relocs == NULL)
		{
			relocs = (Elf32_Rela*)malloc(totbytes);
		}
		else
		{
			relocs = (Elf32_Rela*)realloc(relocs, totbytes);
		}
		*pt_relocs = relocs;
		if (relocs == NULL)
		{
			return false;
		}
		
		// Add the new relocs
		relocs += lastrelocnum;
		for (size_t i = 0; i < addnumrelocs; ++i)
		{			
			relocs[i].r_offset = ReadUInt32_r(fs, &status);
			relocs[i].r_info = ReadUInt32_r(fs, &status);
			relocs[i].r_addend = ReadUInt32_r(fs, &status);
			if (!status) {break;}
		}
	}
	return status;
}

void SortRelocs(Elf32_Rela* relocs, size_t relocnum)
{
	Elf32_Rela t;
	// Just a simple bubble sort.
	for (size_t i = 0; i < relocnum - 1; ++i)
	{
		for (size_t j = i + 1; j < relocnum; ++j)
		{
			if (relocs[i].r_offset > relocs[j].r_offset)
			{
				// flip it
				t = relocs[i];
				relocs[i] = relocs[j];
				relocs[j] = t;
			}
		}
	}
}

bool WriteFixup(FILE* fd, Elf32_Rela* relocs, size_t relocnum)
{
	bool status = true;
	unsigned int lastOffset = 0;
	
	for (size_t i = 0; i < relocnum && status; ++i)
	{
		unsigned short type = relocs[i].r_info & 0xff;
		if (type == R_68K_32)	// The only type we relocate
		{
			// As we are assuming a use of atari-st.ld, then we can also assume
			// that r_offset points to the correct place in the prg file.
			if (lastOffset == 0)
			{
				char* v = (char*)&(relocs[i].r_offset);
				status &= fwrite(v + 3, 1, 1, fd) == 1;
				status &= fwrite(v + 2, 1, 1, fd) == 1;
				status &= fwrite(v + 1, 1, 1, fd) == 1;
				status &= fwrite(v + 0, 1, 1, fd) == 1;
			}
			else
			{
				unsigned int deltaFix = relocs[i].r_offset - lastOffset;
				while (deltaFix > 254)
				{
					char one = 1;
					status &= fwrite(&one, 1, 1, fd) == 1;
					deltaFix -= 254;
				}
				status &= fwrite(&deltaFix, 1, 1, fd) == 1;
			}
			lastOffset = relocs[i].r_offset;
		}
		else if (type != R_68K_PC8 && type != R_68K_PC16 && type != R_68K_PC32) // pc relative is just ignored
		{
			// Not a type we know how to handle!
			printf("Unknown type of relocation data: %x at address: %x\n", relocs[i].r_info & 0xff, relocs[i].r_offset);
			status = false;
		}
	}
	
	// end fixup table.
	if (lastOffset == 0)
	{
		status &= fwrite(&lastOffset, 1, 4, fd) == 4;
	}
	else
	{
		// 1 byte zero to end a fixup table.
		unsigned char justZero = 0;
		status &= fwrite(&justZero, 1, 1, fd) == 1;
	}
	return status;
}
/*
bool OutputSectionReloc(FILE* fs, FILE* fd, Elf32_Shdr* sectionHeader, unsigned int *lastOffset)
{
	bool status = true;
	// Section .rela.text and .rela.data contains relocation data that we want.
	if (sectionHeader->sh_type == SHT_RELA)
	{
		fseek(fs, sectionHeader->sh_offset, SEEK_SET);
		for (size_t i = 0; i < sectionHeader->sh_size; i += sizeof(Elf32_Rela))
		{			
			Elf32_Rela rela;
			rela.r_offset = ReadUInt32_r(fs, &status);
			rela.r_info = ReadUInt32_r(fs, &status);
			rela.r_addend = ReadUInt32_r(fs, &status);
			if (!status) {break;}
			
			switch (rela.r_info & 0xff)	// Type
			{
			case R_68K_32:
				// As we are assuming a use of atari-st.ld, then we can also assume
				// that r_offset points to the correct place in the prg file.
				if (*lastOffset == 0)
				{
					char* v = (char*)&rela.r_offset;
					fwrite(v + 3, 1, 1, fd);
					fwrite(v + 2, 1, 1, fd);
					fwrite(v + 1, 1, 1, fd);
					fwrite(v + 0, 1, 1, fd);
				}
				else
				{
					if (rela.r_offset < *lastOffset)
					{
						// relocation information must be ordered!
						printf("Unordered relocation data!\n");
						status = false;
						break;
					}
					unsigned int deltaFix = rela.r_offset - *lastOffset;
					while (deltaFix > 254)
					{
						char one = 1;
						fwrite(&one, 1, 1, fd);
						deltaFix -= 254;
					}
					fwrite(&deltaFix, 1, 1, fd);
				}
				*lastOffset = rela.r_offset;
				break;
			case R_68K_PC8:
			case R_68K_PC16:
			case R_68K_PC32:
				// As we are assuming the use of atari-st.ld, then we can safely ignore this.
				break;
			default:
				printf("Unknown type of relocation data: %x at address: %x\n", rela.r_info & 0xff, rela.r_offset);
				status = false;
				break;
			}
			if (!status) {break;}
		}
	}
	return status;
}
*/
int main(int argc, char *argv[])
{
	int result = -1;
	if (argc != 3)
	{
		printf("Not correct number of arguments!\n");
		return -1;
	}
	char* source = argv[1];
	char* destination = argv[2];
	
	FILE* fs = NULL;
	FILE* fd = NULL;
	Elf32_Shdr*	sectionHeaders = NULL;
	char* elfStrings = NULL;
	Elf32_Rela* relocs = NULL;
	do
	{
		if ((fs = fopen(source, "rb")) == NULL)
		{
			printf("Could not open %s\n", source);
			break;
		}		
		if ((fd = fopen(destination, "wb")) == NULL)
		{
			printf("Could not open %s\n", destination);
			break;
		}	
		// Read and verify header.
		Elf32_Ehdr header;
		if (!ReadHeader(fs, &header))
		{
			printf("Could not read elf header\n");
			break;
		}		
		
		// Read section headers
		if (NULL == (sectionHeaders = ReadSectionHeaders(fs, &header)))
		{
			printf("Could not read elf section headers\n");
			break;
		}		
		
		// Read strings.
		if (NULL == (elfStrings = ReadSectionData(fs, sectionHeaders, header.e_shstrndx)))
		{
			printf("Could not read elf string section\n");
			break;
		}		
		/*
		for (size_t i = 0; i < header.e_shnum; ++i)
		{
			printf("%s\n", elfStrings + sectionHeaders[i].sh_name);
		}
		*/

		int prgHeaderIndex = FindNamedSection(&header, sectionHeaders, elfStrings, ".prgheader");
		if (prgHeaderIndex < 0)
		{
			printf("Missing .header section\n");
			break;
		}
		int textIndex = FindNamedSection(&header, sectionHeaders, elfStrings, ".text");
		if (textIndex < 0)
		{
			printf("Missing .text section\n");
			break;
		}
		int relaTextIndex = FindNamedSection(&header, sectionHeaders, elfStrings, ".rela.text");
		if (relaTextIndex < 0)
		{
			printf("Missing .rela.text section\n");
			// Not an error, the section can be missing if nothing needs to be relocated.
			//break;	
		}
		int dataIndex = FindNamedSection(&header, sectionHeaders, elfStrings, ".data");
		if (dataIndex < 0)
		{
			printf("Missing .data section\n");
			break;
		}
		int relaDataIndex = FindNamedSection(&header, sectionHeaders, elfStrings, ".rela.data");
		if (relaDataIndex < 0)
		{
			// Not an error, the section can be missing if nothing needs to be relocated.
			printf("Missing .rela.data section\n");
			//break;
		}
		int bssIndex = FindNamedSection(&header, sectionHeaders, elfStrings, ".bss");
		if (bssIndex < 0)
		{
			printf("Missing .bss section\n");
			break;
		}
		
		// When outputting the prg, we assume that the elf have been linked with atari-st.ld
		if (!OutputSectionProgbits(fs, fd, sectionHeaders + prgHeaderIndex))
		{
			printf("Could not handle section .header\n");
			break;
		}
		if (!OutputSectionProgbits(fs, fd, sectionHeaders + textIndex))
		{
			printf("Could not handle section .text\n");
			break;
		}		
		if (!AlignToSectionAddress(fd, sectionHeaders + dataIndex))
		{
			printf("Could not align section .data\n");
			break;
		}
		if (!OutputSectionProgbits(fs, fd, sectionHeaders + dataIndex))
		{
			printf("Could not handle section .data\n");
			break;
		}
		if (!AlignToSectionAddress(fd, sectionHeaders + bssIndex))
		{
			printf("Could not align section .bss\n");
			break;
		}
		
		// BSS section was fixed during linking, so just ignore it.
		
		// Load all relocations
		size_t relocnum = 0;
		if (relaTextIndex >= 0)
		{
			if (!LoadRelocs(fs, sectionHeaders + relaTextIndex, &relocs, &relocnum))
			{
				printf("Could not load section .rela.text\n");
				break;
			}	
		}		
		if (relaDataIndex >= 0)
		{
			if (!LoadRelocs(fs, sectionHeaders + relaDataIndex, &relocs, &relocnum))
			{
				printf("Could not load section .rela.data\n");
				break;
			}			
		}		
		// Sort relocations
		SortRelocs(relocs, relocnum);
		
		if (!WriteFixup(fd, relocs, relocnum))
		{
			printf("Could not write fixup data\n");
			break;
		}			
		
		/*
		unsigned int lastOffset = 0;
		if (!OutputSectionReloc(fs, fd, sectionHeaders + relaTextIndex, &lastOffset))
		{
			printf("Could not handle section .rela.text\n");
			break;
		}		
		if (!OutputSectionReloc(fs, fd, sectionHeaders + relaDataIndex, &lastOffset))
		{
			printf("Could not handle section .rela.data\n");
			break;
		}
		*/


		// And we are done!
		result = 0;
	} while (false);

	if (relocs != NULL)
	{
		free(relocs);
		relocs = NULL;
	}
	if (elfStrings != NULL)
	{
		free(elfStrings);
		elfStrings = NULL;
	}
	if (sectionHeaders != NULL)
	{
		free(sectionHeaders);
		sectionHeaders = NULL;
	}
	if (fs != NULL)
	{
		fclose(fs);
		fs = NULL;
	}		
	if (fd != NULL)
	{
		fclose(fd);
		fd = NULL;
	}		
	return result;
}