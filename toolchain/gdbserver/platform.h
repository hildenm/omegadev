/*
	WIP, unused for now.
	Will in the future provide an interface for supporting multiple platforms.
*/

#ifndef PLATFORM_DEFINED
#define PLATFORM_DEFINED

#ifdef __cplusplus
extern "C" {
#endif

typedef int (*_InitExceptions)(void) _InitExceptions;
typedef void (*_RestoreExceptions)(void) _RestoreExceptions;
typedef void _Exception(int num, int* si_signo, int* si_code) _Exception;

typedef int (*_InitComm)(void* settings) _InitComm;
typedef void (*_RestoreComm)(void) _RestoreComm;
int GetByte(void)
void PutByte(char ch)

typedef unsigned char* (*_GetInferiorMemoryAddress)(unsigned char* address) _GetInferiorMemoryAddress;
typedef void (*_StoreMemoryRegisters)(unsigned int inferior) _StoreMemoryRegisters;
typedef void (*_LoadMemoryRegisters)(unsigned int inferior) _LoadMemoryRegisters;

int LoadInferior(const char* fileName, const char* cmdLine, const char* environment)
bool RunInferior(void)
void __attribute__ ((noreturn)) TerminateInferior(int si_signo)

typedef struct
{
	unsigned short	BreakPoint;

	_InitExceptions InitExceptions;
	_RestoreException RestoreException;
	_Exception Exception;

	_InitComm InitComm;
	_RestoreComm RestoreComm;
	
	_GetInferiorMemoryAddress GetInferiorMemoryAddress;
	_StoreMemoryRegisters StoreMemoryRegisters;
	_LoadMemoryRegisters LoadMemoryRegisters;
	
} Platform;



#ifdef __cplusplus
}
#endif

#endif // PLATFORM_DEFINED

