#!/bin/bash

# Target specific settings
TARGET=m68k-elf
PREFIX="/usr/local/bin/$TARGET"
PATH=$PATH:$PREFIX/bin

# Get number of cores/threads for this system
BUILD_THREADS=$(nproc)

# Build Atari libgloss and linker scripts
cd libgloss/atari_tos
make clean
make -j$BUILD_THREADS TOOLKIT=$PREFIX all
make install
cd ../..

# Build m68k-elf-prg (elf to prg converter)
cd elf-prg
make clean
make -j$BUILD_THREADS M68K_TOOLKIT=$PREFIX all
make install
cd ..

# Build gdbserver
# If this succeeds, then everything above have probably worked too.
cd gdbserver
make clean
make -j$BUILD_THREADS TOOLKIT=$PREFIX all
make install
cd ..

# Done!
