#include "gem_errno.h"
#include "traps.h"

/*
	According to the newlib documentation, this function should be used to rename a file.
	But posix uses this function to make a hard link to a file...
	So the safe way is to return -1 and hook into the clib function instead.
*/

int link(char *old, char *new)
{
	/*
	int err = trap1_56(old, new);
	if (err <  0)
	{
		gem_error_to_errno(err);
		return -1;
	}
	return 0;
	*/
	return -1;
}

int _rename(const char *old_filename, const char *new_filename)
{
	int err = trap1_56(old_filename, new_filename);
	if (err <  0)
	{
		gem_error_to_errno(err);
		return -1;
	}
	return 0;
}