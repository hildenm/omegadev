#include <_ansi.h>
#include "gem_errno.h"
#include "traps.h"

int close(int fd)
{
	int err = GEM_EIHNDL;
	if (fd >= 0)
	{
		err = trap1_3e((unsigned short)fd);
	}
	if (err <  0)
	{
		gem_error_to_errno(err);
		return -1;
	}
	return 0;
}
