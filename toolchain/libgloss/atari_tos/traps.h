#ifndef TRAPS_DEFINED
#define TRAPS_DEFINED

#include "gem_basepage.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
	All trap functions have defenitions for their named calls and their trap number calls.
	They are named and defined in the way it is in Atari documentation.
	All functions have not been added yet.
*/

#define Cauxin trap1_3
short trap1_3(void);

#define Cauxout trap1_4
void trap1_4(short ch);

#define Cconws trap1_9
int trap1_9(const char* text);

#define Dsetdrv trap1_e
unsigned int trap1_e(unsigned short bios_drive);

#define DEV_READY	-1
#define DEV_BUSY	0
#define Cauxis trap1_12
short trap1_12(void);

#define Cauxos trap1_13
short trap1_13(void);

// Returned drive starts at 0 for A
#define Dgetdrv trap1_19
unsigned short trap1_19(void);

#define Tgetdate trap1_2a
unsigned short trap1_2a(void);

#define Tsetdate trap1_2b
int trap1_2b(unsigned short date);

#define Tgettime trap1_2c
unsigned short trap1_2c(void);

#define Tsettime trap1_2d
int trap1_2d(unsigned short time);

#define Fgetdta trap1_2f
struct DTA* trap1_2f(unsigned short bios_handle);

#define Dcreate trap1_39
int trap1_39(const char* bios_path);

#define Ddelete trap1_3a
int trap1_3a(const char* bios_path);

#define Dsetpath trap1_3b
int trap1_3b(const char* bios_path);

#define Fcreate trap1_3c
int trap1_3c(const char* bios_path, unsigned short bios_attrib);

#define Fopen trap1_3d
int trap1_3d(const char* bios_path, unsigned short bios_mode);

#define GSH_BIOSCON		0xFFFF
#define GSH_BIOSAUX		0xFFFE
#define GSH_BIOSPRN		0xFFFD
#define GSH_BIOSMIDIIN	0xFFFC
#define GSH_BIOSMIDIOUT	0xFFFB
#define GSH_CONIN		0x00
#define GSH_CONOUT		0x01
#define GSH_AUX			0x02
#define GSH_PRN			0x03

#define Fclose trap1_3e
int trap1_3e(unsigned short bios_handle);

#define Fread trap1_3f
int trap1_3f(unsigned short bios_handle, int length, void* buf);

#define Fwrite trap1_40
int trap1_40(unsigned short bios_handle, int length, const void* buf);

#define Fdelete trap1_41
int trap1_41(const char* bios_path);

#define Fseek trap1_42
int trap1_42(unsigned int file_position, unsigned short bios_handle, unsigned short bios_mode);

// bios_drive 0 is default drive, and 1 and upwards is A...
#define Dgetpath trap1_47
int trap1_47(char* buf, unsigned short bios_drive);

#define Mfree trap1_49
int trap1_49(void* start_addr);

#define PE_LOADGO		0
#define PE_LOAD			3
#define PE_GO			4
#define PE_BASEPAGE		5
#define PE_GOTHENFREE	6
#define Pexec trap1_4b
int trap1_4b(unsigned short mode, const char* file_name, const char* cmdline, const char* envstring);

#define Pterm trap1_4c
void __attribute__ ((noreturn)) trap1_4c(unsigned short retcode);

#define Frename trap1_56
int trap1_56(const char* oldname, const char* newname);


#define DEV_PRINTER		0
#define DEV_AUX			1
#define DEV_CONSOLE		2
#define DEV_CON			2
#define DEV_MIDI		3
#define DEV_IKBD		4
#define DEV_RAW			5
#define Bconstat trap13_1
int trap13_1(unsigned short dev);

#define Bconin trap13_2
unsigned int trap13_2(unsigned short dev);

#define Bconout trap13_3
unsigned int trap13_3(unsigned short dev, unsigned short ch);

#define Bcostat trap13_8
int trap13_8(unsigned short dev);


// speed
#define BAUD_19200		0
#define BAUD_9600		1
#define BAUD_4800		2
#define BAUD_3600		3
#define BAUD_2400		4
#define BAUD_2000		5
#define BAUD_1800		6
#define BAUD_1200		7
#define BAUD_600		8
#define BAUD_300		9
#define BAUD_200		10
#define BAUD_150		11
#define BAUD_134		12
#define BAUD_110		13
#define BAUD_75			14
#define BAUD_50			15
#define BAUD_INQUIRE	0xfffe
#define RS_LASTBAUD		0xfffe
// flow
#define FLOW_NONE		0
#define FLOW_SOFT		1
#define FLOW_HARD		2
#define FLOW_BOTH		3
// ucr
#define RS_ODDPARITY	0x0002
#define RS_EVENPARITY	0x0000
#define RS_PARITYENABLE	0x0004
#define RS_NOSTOP		0x0000
#define RS_1STOP		0x0008
#define RS_15STOP		0x0010
#define RS_2STOP		0x0018
#define RS_8BITS		0x0000
#define RS_7BITS		0x0020
#define RS_6BITS		0x0040
#define RS_5BITS		0x0060
#define RS_CLK16		0x0080
// rsr, tsr
#define RS_RECVENABLE	0x0001
#define RS_SYNCSTRIP 	0x0002
#define RS_MATCHBUSY 	0x0004
#define RS_BRKDETECT 	0x0008
#define RS_FRAMEERR  	0x0010
#define RS_PARITYERR 	0x0020
#define RS_OVERRUNERR	0x0040
#define RS_BUFFULL   	0x0080
// ucr, rsr, tsr, scr
#define RS_INQUIRE   	0xffff
#define Rsconf trap14_f
int trap14_f(unsigned short speed, unsigned short flow, unsigned short ucr, unsigned short rsr, unsigned short tsr, unsigned short scr);


#define GI_FLOPPYSIDE	0x01
#define GI_FLOPPYA		0x02
#define GI_FLOPPYB		0x04
#define GI_RTS			0x08
#define GI_DTR			0x10
#define GI_STROBE		0x20
#define GI_GPO			0x40
#define GI_SCCPORT		0x80
#define Offgibit trap14_1d
void trap14_1d(unsigned short mask);

#define Ongibit trap14_1e
void trap14_1e(unsigned short mask);

#define Supexec trap14_26
int trap14_26(int (*callback)(void));

#ifdef __cplusplus
}
#endif

#endif // TRAPS_DEFINED
