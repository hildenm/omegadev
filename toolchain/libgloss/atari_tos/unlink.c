#include <_ansi.h>
#include "gem_errno.h"
#include "traps.h"

int unlink(char * path)
{
	int err = trap1_41(path);
	if (err <  0)
	{
		gem_error_to_errno(err);
		return -1;
	}
	return 0;
}
