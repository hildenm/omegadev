
	.text
	
	.global __start
__start:
	move.l	4(a7), _BasePage
	lea		__stack, a7		| __stack is defined in atari-st-micro.ld

	moveq	#0, d0
	move.l	d0, -(a7)		| argv
	move.l	d0, -(a7)		| argc
	jsr		main
	lea		8(a7), a7
	move.w	d0,-(a7)
	move.w	#0x4c,-(a7)
	trap	#1

	.even
	
	.bss
	
	.even
	.global _BasePage
	.lcomm 	_BasePage, 4
	.even
