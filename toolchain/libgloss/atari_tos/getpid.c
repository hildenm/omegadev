#include <_ansi.h>

/*
 * getpid -- only one process, so just return 1.
 */
int getpid(void)
{
	return 1;
}
