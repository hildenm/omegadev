#include <unistd.h>
#include <errno.h>

#include <_ansi.h>
#include "gem_errno.h"
#include "traps.h"

// std functions that will be there when we link later on.
void* malloc(size_t);
void free(void*);

char *getcwd(char *buf, size_t size)
{
	char* retbuf = buf;
	if (retbuf == 0)
	{
		if (size <= 0)
		{
			size = 128;
		}
		// allocate memory
		retbuf = (char*)malloc(size);
		if (retbuf == 0)
		{
			errno = EIO;
			return 0;
		}
	}
	
	unsigned short drive = trap1_19();
	int result = trap1_47(retbuf + 2, drive + 1);
	if (result < 0)
	{
		gem_error_to_errno(result);
		if (retbuf != buf)
		{
			// free memory
			free(retbuf);
		}
		return 0;
	}
	retbuf[0] = (char)('A' + drive);
	retbuf[1] = ':';
	return retbuf;
}

// Code below is not supported by the newlib posix version.
/*

char *getwd(char *buf)
{
	return getcwd(buf, 128);	// Max path of atari st.
}

char *get_current_dir_name(void)
{
	char buf[129];
	if (getcwd(buf, 128) == 0)
	{
		// errno already set by getcwd
		return 0;
	}
	
	int str_len = 0;
	while (buf[str_len] != 0) {++str_len;}
	if (buf[str_len - 1] == '\\') {buf[--str_len] = 0;}
	int last_divider = str_len;
	while (buf[last_divider] != '\\') {--last_divider;}

	// allocate memory
	int namesize = str_len - last_divider; // includes last divider
	char* retbuf = (char*)malloc(namesize);
	if (retbuf == 0)
	{
		// errno is already set in malloc
		return 0;
	}
	for (int i = 0; i < namesize - 1; ++i)
	{
		retbuf[i] = buf[last_divider + 1 + i];
	}
	retbuf[namesize] = 0;
	
	return retbuf;
}
*/