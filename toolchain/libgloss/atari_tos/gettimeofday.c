#include <sys/time.h>
#include "gem_errno.h"
#include "traps.h"

int gettimeofday(struct timeval* tv, void* __tz)
{
	struct timezone* tz = __tz;
	if (tz != 0)
	{
		// Support for timezone have been removed from linux glibc, so we just fill in a zero timezone.
		tz->tz_minuteswest = 0;
		tz->tz_dsttime = 0;	
	}
	if (tv != 0)
	{
		unsigned short date = trap1_2a();
		unsigned short time = trap1_2c();
		
		struct tm t = { 0 };
		t.tm_year = ((date >> 9) & 0x7f) + 80;	// Years since 1900
		t.tm_mon = ((date >> 5) & 0xf) - 1;
		t.tm_mday = date & 0x1f;
		t.tm_hour = ((time >> 11) & 0x1f);
		t.tm_min = ((time >> 5) & 0x3f);
		t.tm_sec = (time & 0x1f) * 2;
		t.tm_isdst = -1;
		
		tv->tv_sec = mktime(&t);
		tv->tv_usec = 0;
	}
	return 0;
}

int settimeofday(const struct timeval* tv, const struct timezone* tz)
{
	// Support for timezone have been removed from linux glibc, so we just ignore it.
	if (tv != 0)
	{
		struct tm* t = gmtime(&tv->tv_sec);
		if (t == 0)
		{
			// gmtime have already set errno
			return -1;
		}
		if (t->tm_year < 80 || t->tm_year > (80 + 0x7f))
		{
			// TOS cannot handle years before 1980 or after 2107
			gem_error_to_errno(GEM_EBADRQ);
			return -1;
		}
		unsigned short date = (unsigned short)(((t->tm_year - 80) & 0x7f) << 9);
		date |= (unsigned short)(((t->tm_mon + 1) & 0xf) << 5);
		date |= (unsigned short)(t->tm_mday & 0x1f);
		unsigned short time = (unsigned short)((t->tm_hour & 0x1f) << 11);
		time |= (unsigned short)((t->tm_min & 0x3f) << 5);
		time |= (unsigned short)((t->tm_sec & 0x3e) >> 1);

		int err;
		if ((err = trap1_2b(date)) < 0)
		{
			gem_error_to_errno(err);
			return -1;
		}
		if ((err = trap1_2d(time)) < 0)
		{
			gem_error_to_errno(err);
			return -1;
		}
	}	
	return 0;
}