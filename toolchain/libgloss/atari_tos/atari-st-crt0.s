
	.text
	.global _HeapSize	| atari.ld defaults this to 0 if not declared in user code.
	
	.global __start
__start:
	/* 
		__start have the elf address of 0.
		So telling gdb to offset symbol addresses with the loaded address of __start will get the correct symbol address.
	*/
	move.l	4(a7),a0
	move.l	a0, _BasePage
	lea		__stack, a7		| __stack is defined in atari.ld
	move.l	a7, d0
	move.l	d0, _HeapBottom
	move.l	d0, _HeapPtr
	move.l	4(a0), _HeapTop
	| Use all memory if nothing else is specified.
	
	tst.l	_HeapSize
	beq.s	1f
	add.l	_HeapSize, d0
	sub.l	(a0), d0			| d0 is now the TPA size

	| Setup stack and tell GEM that we are happy with the memory we got.
	move.l	d0, -(a7)
	move.l	a0, -(a7)
	clr.w	-(a7)
	move.w	#0x4a, -(a7)	| Mshrink()
	trap	#1
	lea		12(a7), a7
	tst.l	d0
	bmi		_early_exit
1:

	lea		__CTOR_LIST__, a0
	bsr		__execute_list

	bsr		__call_main_with_argc_argv
	
	move.l	d0, -(a7)
	jsr 	exit
	/*
		There should not be any possibillity of reaching here.
		So we put an illegal here to tell us if something bugs out.
	*/
	illegal
	
__execute_list:
	move.l	(a0)+, d0
	bra.s	2f
1:
	move.l	(a0)+, a1
	move.l	a0, -(a7)
	move.l	d0, -(a7)
	jsr		(a1)
	move.l	(a7)+, d0
	move.l	(a7)+, a0
2:
	dbra	d0, 1b
	rts

/*
	Need to implement "The Atari Extended Argument Specification".
	Also, there is no guarantee that the command line is zero terminated.
*/
__call_main_with_argc_argv:
	move.l	a7, a6
	moveq	#0, d0
	moveq	#0, d2
	movea.l	_BasePage, a0
	lea		128(a0), a0		| add offset to the cmdline
	lea		_cmdline, a1
	move.b	(a0)+, d2		| d2 contains number of bytes in command line (max 127)
	beq.s	3f
	clr.b	(a1, d2.w)		| end our decoded args with a zero.
	bra.s	2f
1:
	move.b	(a0, d2.w), d1
	cmp.b	#' ', d1
	bne.s	4f
	moveq	#0, d1
	lea		1(a1, d2.w), a2
	tst.b	(a2)
	beq.s	4f
	move.l	a2, -(a7)
	addq.w	#1, d0
4:	
	move.b	d1, (a1, d2.w)
2:
	subq.w	#1, d2
	bcc.s	1b
	move.l	a1, -(a7)
	addq.w	#1, d0
3:
/*
	This code do not work.
	The DTA overlaps the memory space taken by the Basepage command line,
	so it is basically garbage data and do not contain the process filename we want.
	Keeping the code here so no one else needs to do the same mistake.
	
	movea.l	_BasePage, a0
	move.l	32(a0), a0		| DTA
	lea		30(a0), a0		| filename
	move.l	a0, -(a7)
*/
	pea		procname		| first argument is always the proc name. That we do not know...
	addq.w	#1, d0
	move.l	a7, a5
	move.l	a6, -(a7)		| To know where to move it back again.
	
	move.l	a5, -(a7)		| argv
	move.l	d0, -(a7)		| argc
	jsr		main
	move.l	8(a7), a7		| move it back.
	
	rts
	
/*
	_exit is called from exit, and it is not expected to return.
*/
	.global _exit
_exit:
	lea		__DTOR_LIST__, a0
	bsr		__execute_list
_early_exit:
	move.l 4(a7), d0
	move.w	d0,-(a7)
	move.w	#0x4c,-(a7)
	trap	#1

.ifdef MINI_CRT

/*
	We don not have any exit/atexit without libc, so we skip ahead and jump directly to _exit 
*/
exit:
	jmp _exit
	
/*
	We need to supply a __errno function for our process (only got one).
	The function is simple: just return a pointer to the place where errno should be stored.
*/
	.global __errno
__errno:
	move.l	#errno_code, d0
	rts
.endif

	.even
	.data
procname:
	.asciz	"yourapp.lol"
	.even
	.bss
	
	.even
	.global	__dso_handle
	.lcomm	__dso_handle, 4
.ifdef MINI_CRT
	.global	errno_code
	.lcomm	errno_code, 4
.endif
	.global _BasePage
	.lcomm 	_BasePage, 4
	.lcomm	_cmdline, 128
	.global _HeapPtr
	.lcomm 	_HeapPtr, 4
	.global _HeapBottom
	.lcomm 	_HeapBottom, 4
	.global _HeapTop
	.lcomm 	_HeapTop, 4
	.even
