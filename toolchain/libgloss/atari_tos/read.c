#include <unistd.h>
#include <_ansi.h>
#include "gem_errno.h"
#include "traps.h"

_READ_WRITE_RETURN_TYPE read(int fd, void *buf, size_t nbytes)
{
	int numRead = GEM_EIHNDL;
	if (fd >= 0)
	{
		numRead = trap1_3f((unsigned short)fd, nbytes, buf);
	}
	if (numRead <  0)
	{
		gem_error_to_errno(numRead);
		return -1;
	}
	return numRead;
}
