#!/bin/bash

# Before running this, make sure that all neccessary packets are installed.
# Read "ReadMe-build.txt" for information about neccessary packets.

# Target specific settings
TARGET=m68k-elf
PREFIX="/usr/local/bin/$TARGET"
PATH=$PATH:$PREFIX/bin

# Versions to download and build.
BINUTIL_VERSION="2.41"
GCC_VERSION="13.2.0"
NEWLIB_VERSION="4.4.0.20231231"
#GDB_VERSION="14.1"

# Get number of cores/threads for this system
BUILD_THREADS=$(nproc)

# Find out shared library extension
. /etc/os-release
SHARED_EXT="so"
if [ $NAME == "MSYS2" ]
then
	SHARED_EXT="dll"
fi

# Make gcc toolchain build dir and enter
mkdir -p build
cd build

# Get binutils sources
if [ ! -d binutils-$BINUTIL_VERSION ]; then
	wget https://ftp.gnu.org/gnu/binutils/binutils-$BINUTIL_VERSION.tar.bz2
	tar -xmf binutils-$BINUTIL_VERSION.tar.bz2
fi

# Get gcc sources
if [ ! -d gcc-$GCC_VERSION ]; then
	wget https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz
	tar -xmf gcc-$GCC_VERSION.tar.gz
	cd gcc-$GCC_VERSION
	./contrib/download_prerequisites
	cd ..
fi

# Get newlib sources
if [ ! -d newlib-$NEWLIB_VERSION ]; then
	wget ftp://sourceware.org/pub/newlib/newlib-$NEWLIB_VERSION.tar.gz
	tar -xmf newlib-$NEWLIB_VERSION.tar.gz
	# Apply patches that fixes comilation errors.
	patch newlib-$NEWLIB_VERSION/libgloss/read.c ../libgloss/read.patch
	patch newlib-$NEWLIB_VERSION/libgloss/write.c ../libgloss/write.patch
fi

## Get GDB sources
#if [ ! -d gdb-$GDB_VERSION ]; then
#	wget https://ftp.gnu.org/gnu/gdb/gdb-$GDB_VERSION.tar.gz
#	tar -xmf gdb-$GDB_VERSION.tar.gz
#fi

# build binutils
mkdir -p b-binutils
cd b-binutils
../binutils-$BINUTIL_VERSION/configure --prefix=$PREFIX --target=$TARGET --enable-lto --with-sysroot --disable-nls --disable-werror
make -j$BUILD_THREADS
make install-strip
cd ..

# build gcc
mkdir -p b-gcc
cd b-gcc
../gcc-$GCC_VERSION/configure --prefix=$PREFIX --target=$TARGET \
--disable-multilib \
--disable-libssp \
--with-cpu=m68000 \
--disable-nls \
--enable-languages=c,c++ \
--with-newlib \
--disable-shared \
--disable-thread \
--enable-lto \
--enable-target-optspace \
--with-headers=../newlib-$NEWLIB_VERSION/newlib/libc/include
# --with-arch=m68k # All 68K cpu's except coldfire.
make -j$BUILD_THREADS
make install-strip
cd ..

# build newlib
mkdir -p b-newlib
cd b-newlib
../newlib-$NEWLIB_VERSION/configure --prefix=$PREFIX --target=$TARGET \
--disable-multilib \
--with-cpu=m68000 \
--with-float=soft \
--enable-target-optspace \
--disable-newlib-wide-orient \
--disable-newlib-mb \
--disable-newlib-fvwrite-in-streamio \
--disable-newlib-fseek-optimization \
--enable-newlib-nano-malloc \
--disable-newlib-unbuf-stream-opt \
--disable-newlib-multithread \
--disable-newlib-io-float \
--enable-newlib-nano-formatted-io
make -j$BUILD_THREADS
make install
cd ..

# Make ar and g++ able to find the lto plugin
yes | cp -rf $PREFIX/libexec/gcc/m68k-elf/$GCC_VERSION/liblto_plugin.$SHARED_EXT $PREFIX/lib/bfd-plugins/liblto_plugin.$SHARED_EXT

## build GDB
#mkdir -p b-gdb
#cd b-gdb
#../gdb-$GDB_VERSION/configure --prefix=$PREFIX --target=$TARGET
#make -j$BUILD_THREADS
#make install-strip
#cd ..

# exit gcc build dir and ten continue with Atari specific stuff
cd ..

# Build Atari libgloss and linker scripts
cd libgloss/atari_tos
make clean
make -j$BUILD_THREADS TOOLKIT=$PREFIX all
make install
cd ../..

# Build m68k-elf-prg (elf to prg converter)
cd elf-prg
make clean
make -j$BUILD_THREADS M68K_TOOLKIT=$PREFIX all
make install
cd ..

# Build m68k-find-remote (Helper program that can be omitted if not wanted. Used for IDE to find build destination.)
#cd find-remote
#make -j$BUILD_THREADS
#make install
#cd ..

# Build gdbserver
# If this succeeds, then everything above have probably worked too.
cd gdbserver
make clean
make -j$BUILD_THREADS TOOLKIT=$PREFIX all
make install
cd ..

# Done!