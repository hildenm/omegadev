# OmegaDev toolchain
The folder "omegadev/toolchain" contains all scripts and code to build a complete toolchain with all necessary libraries and tools needed to build and debug Atari ST software.  
See the [wiki](https://bitbucket.org/hildenm/omegadev/wiki/) for information about how to build the toolchain.  

## Directory contents

### libgloss/  
Contains all machine specific code for building the "newlib" needed "libgloss".  
Also contains the link scripts that do the linking of executables into a Atari gem/tos friendly "m68k-elf" format.

### elf-prg/
Contains the code for the tool that converts a "m68k-elf" formatted executable into a Atari gem/tos executable.

### find-remote/
Don't care for now, will probably be removed in the future.

### gdbserver/
This contains all code for building the "gdbsrv.ttp" tool that can be run on an Atari ST to enable remote debugging using GDB.  
Even though this folder is called "gdbserver", it has none of the code from the gdb projects gdbserver.  
It is only called "gdbserver" because it tries to do the same job as the original do.  
"gdbsrv.ttp" is original to this project and specifically made for Atari ST.

### ReadMe-build.txt
A short description of how to build the toolchain.  
Beginners are recomended to use the [wiki](https://bitbucket.org/hildenm/omegadev/wiki/) for a better source of information about the subject.  

### build-toolchain.sh
A script that will download and build everything needed for the OmegaDev toolchain.  

### rebuild-minimal.sh
A script for rebuilding libgloss, elf-prg and gdbserver.  
Many times faster than doing a complete rebuild of the toolchain.

## License

This project is licensed under the MIT License (MIT) - see the [LICENSE.md](https://bitbucket.org/hildenm/omegadev/src/master/LICENSE.md) file for details

