#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define maxLine 1024

#if __GNUC__ && !__MINGW32__

// Running on linux
int FindFile(const char* fileToFind, char* foundPath)
{
	int result = 0;
	FILE* proc_mounts = fopen("/proc/mounts", "rb");
	if (proc_mounts != NULL)
	{
		char line[maxLine];
		char devName[maxLine];
		char mountPath[maxLine];
		char formatName[maxLine];
		while (fgets(line, maxLine, proc_mounts))
		{
			// We are only interested in those lines who starts with "/dev/sd"
			if (strncmp("/dev/sd", line, 7) == 0)
			{
				if (3 == sscanf(line, "%s%s%s", devName, mountPath, formatName))
				{
					// We know that all ST drives are exposed as vfat to the host.
					if (strcmp("vfat", formatName) == 0)
					{
						// We may have something to work with.
						sprintf(foundPath, "%s/%s", mountPath, fileToFind);
						FILE* fs = fopen(foundPath, "rb");
						if (fs != NULL)
						{
							fclose(fs);
							result = 1;
							break;
						}
					}
				}
				
			}
		}
		fclose(proc_mounts);
	}
	return result;
}

#endif // __GNUC__ && !__MINGW32__

#if __GNUC__ && __MINGW32__

// Running on windows using mingw	
int FindFile(const char* fileToFind, char* foundPath)
{
	// Just try every drive letter... Well, we skip a-b as we know they can't be it.
	int result = 0;
	char mountPath[2];
	for (int d = 'c'; d <= 'z'; ++d)
	{
		mountPath[0] = (char)d;
		mountPath[1] = 0;
		//sprintf(foundPath, "/%s/%s", mountPath, fileToFind);
		sprintf(foundPath, "%s:\\%s", mountPath, fileToFind);
		FILE* fs = fopen(foundPath, "rb");
		if (fs != NULL)
		{
			fclose(fs);
			result = 1;
			break;
		}
	}
	return result;
}

#endif // __GNUC__ && __MINGW32__

int main(int argc, char *argv[])
{
	int result = -1;
	if (argc != 2)
	{
		printf("Not correct number of arguments!\n");
		return -1;
	}
	// Get root path and name of remote config file to find.
	char* fileToFind = argv[1];
	char foundPath[maxLine];
	if (FindFile(fileToFind, foundPath) != 0)
	{
		printf("%s", foundPath);
		result = 0;
	}
	return result;
}
