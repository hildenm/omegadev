# OmegaDev examples
The folder "omegadev/examples" contains various example projects and helper files.  
See the [wiki](https://bitbucket.org/hildenm/omegadev/wiki/) for information about how to build and debug examples.  

## Directory contents

### dbtest/  
Very simple example that only flashes background color. Have been used to test the "gdbsrv.ttp" program.  

### dots/
Sinus modulated dots. Shows how to combine C and assembler.

### libtest/
Not example code as such, but used to test if cross compiling standard libraries are working.  
Currently, libstdc++ do not work.  

### makefile.inc/
Makefile include file containing all specialized compiler and linker settings for generating Atari ST code.  

## License

This project is licensed under the MIT License (MIT) - see the [LICENSE.md](https://bitbucket.org/hildenm/omegadev/src/master/LICENSE.md) file for details

