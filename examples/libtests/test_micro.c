/*
	We only have language and no library support.
	Whatever you want to do, do it yourself!
*/

// Code copied from AtariCrt/traps.c
#define TRAP_BEGIN "move.l		%%a7, %%a3\n\t"
#define TRAP_FUNC(num, func) "move.w		#" #func ", %%a7@-\n\ttrap		#" #num "\n\tmove.l		%%a3, %%a7\n\t"
#define CLOBBER_REG "d1", "d2", "a0", "a1", "a2", "a3"

int trap14_26(int (*callback)(void))
{
	int callback_return = -1;
	__asm__(
		TRAP_BEGIN
		"move.l		%1, %%a7@-\n\t"
		TRAP_FUNC(14, 0x26)
		: "=r" (callback_return) 
		: "g" (callback)
		: CLOBBER_REG);
	return callback_return;
}

int SuperMain(void)
{
	unsigned short* palette = (unsigned short*)0xffff8240;
	palette[0] = 0xf88;		// You got pink screen!
	return 0;
}
int main(void)
{
	return trap14_26(SuperMain);
}