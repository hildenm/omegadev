#include <unistd.h>
#include <fcntl.h>
#include <traps.h>

extern "C" void print(const char *ptr);	// Which include file is this declared in?

int SuperMain(void)
{
	unsigned short* palette = (unsigned short*)0xffff8240;
	palette[0] = 0xf88;		// You got pink screen!
	return 0;
}

int main(int argc, char** argv)
{
	trap14_26(SuperMain);
    print("Hello World");
	int h = open("a.txt", O_RDWR | O_CREAT);
	write(h, "hello world\r\n", 13);
	int len = lseek(h, 0, SEEK_CUR);
	lseek(h, 0, SEEK_SET);
	char inbuf[100];
	read(h,inbuf, len);
	close(h);
    return 0;
}