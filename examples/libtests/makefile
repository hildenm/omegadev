# Project source and destination settings
TARGET_NAME := test
BUILD_DIR := build
SRC_DIR := .
SOURCES_FULL := test_full.c test_full1.cpp
SOURCES_LIM := test_lim.c test_lim1.cpp
SOURCES_MINI := test_mini.c
SOURCES_MICRO := test_micro.c

# Project build architecture settings
CPU := 68000
LINK_SCRIPT_FULL := atari-st-fullc++.ld
LINK_SCRIPT_LIM := atari-st-limc++.ld
LINK_SCRIPT_MINI := atari-st-mini.ld
LINK_SCRIPT_MICRO := atari-st-micro.ld
TOOLKIT	:= /usr/local/bin/m68k-elf
GCC_LIB := $(TOOLKIT)/lib/gcc/m68k-elf/13.2.0

# Toolkit executables, libraries and directories settings
TOOLKIT_BIN	:= $(TOOLKIT)/bin
NEWLIB_LIB := $(TOOLKIT)/m68k-elf/lib
CC := $(TOOLKIT_BIN)/m68k-elf-g++
CXX := $(TOOLKIT_BIN)/m68k-elf-g++
AS := $(TOOLKIT_BIN)/m68k-elf-as
LD := $(TOOLKIT_BIN)/m68k-elf-g++
AR := $(TOOLKIT_BIN)/m68k-elf-ar
ELFTOPRG := $(TOOLKIT_BIN)/m68k-elf-prg
TOOLKIT_INCLUDE := $(TOOLKIT)/include/atari_tos

# Compiler and linker flags (be careful!)
CFLAGS := -I$(TOOLKIT_INCLUDE) -Wall -Os -ggdb -m$(CPU) -msoft-float -flto
CXXFLAGS := -std=c++17 -fno-exceptions -fno-rtti
ASFLAGS := -gstabs+ --register-prefix-optional -mcpu=$(CPU)
LDFLAGS := -nostdlib -L $(GCC_LIB) -L $(NEWLIB_LIB) -Xlinker --emit-relocs -Xlinker --no-warn-rwx-segments -static
#--enable-newlib-io-float when configure gcc, to use the link line below and have float support in printf/scanf
#LDFLAGS := -nostdlib -L $(GCC_LIB) -L $(NEWLIB_LIB) -Xlinker --emit-relocs -static -Xlinker -u -Xlinker _printf_float -Xlinker -u -Xlinker _scanf_float

TARGET_FULL_PRG := $(BUILD_DIR)/$(TARGET_NAME)_full.prg
TARGET_FULL_ELF := $(BUILD_DIR)/$(TARGET_NAME)_full
TARGET_LIM_PRG := $(BUILD_DIR)/$(TARGET_NAME)_lim.prg
TARGET_LIM_ELF := $(BUILD_DIR)/$(TARGET_NAME)_lim
TARGET_MINI_PRG := $(BUILD_DIR)/$(TARGET_NAME)_mini.prg
TARGET_MINI_ELF := $(BUILD_DIR)/$(TARGET_NAME)_mini
TARGET_MICRO_PRG := $(BUILD_DIR)/$(TARGET_NAME)_micro.prg
TARGET_MICRO_ELF := $(BUILD_DIR)/$(TARGET_NAME)_micro

# Building object list
OBJS_FULL := $(foreach source,$(SOURCES_FULL),$(BUILD_DIR)/$(basename $(source)).o)
OBJS_LIM := $(foreach source,$(SOURCES_LIM),$(BUILD_DIR)/$(basename $(source)).o)
OBJS_MINI := $(foreach source,$(SOURCES_MINI),$(BUILD_DIR)/$(basename $(source)).o)
OBJS_MICRO := $(foreach source,$(SOURCES_MICRO),$(BUILD_DIR)/$(basename $(source)).o)

# Make prg targets
$(TARGET_FULL_PRG): $(TARGET_FULL_ELF)
	$(ELFTOPRG) $< $@

$(TARGET_LIM_PRG): $(TARGET_LIM_ELF)
	$(ELFTOPRG) $< $@

$(TARGET_MINI_PRG): $(TARGET_MINI_ELF)
	$(ELFTOPRG) $< $@

$(TARGET_MICRO_PRG): $(TARGET_MICRO_ELF)
	$(ELFTOPRG) $< $@

# Make elf targets
$(TARGET_FULL_ELF): $(OBJS_FULL)
	$(LD) $(LDFLAGS) $^ -o $@ -Xlinker -T -Xlinker ldscripts/$(LINK_SCRIPT_FULL)

$(TARGET_LIM_ELF): $(OBJS_LIM)
	$(LD) $(LDFLAGS) $^ -o $@ -Xlinker -T -Xlinker ldscripts/$(LINK_SCRIPT_LIM)

$(TARGET_MINI_ELF): $(OBJS_MINI)
	$(LD) $(LDFLAGS) $^ -o $@ -Xlinker -T -Xlinker ldscripts/$(LINK_SCRIPT_MINI)

$(TARGET_MICRO_ELF): $(OBJS_MICRO)
	$(LD) $(LDFLAGS) $^ -o $@ -Xlinker -T -Xlinker ldscripts/$(LINK_SCRIPT_MICRO)

# assembly
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.s
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) $(CXXFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c $< -o $@

# Create build directory
$(BUILD_DIR): $(shell mkdir -p $(BUILD_DIR))

.PHONY: clean

full: $(TARGET_FULL_PRG)

lim: $(TARGET_LIM_PRG)

mini: $(TARGET_MINI_PRG)

micro: $(TARGET_MICRO_PRG)

all: full lim mini micro

clean:
	$(shell rm -r $(BUILD_DIR))
