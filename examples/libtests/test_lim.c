#include <stdio.h>
#include <stdlib.h>

void CallFromC(void);


void afunc(void)
{
    printf("Goodbye World");
}

int main(int argc, char** argv)
{
	// Need some random numbers so compiler doesn't optimize away code.
	// Pick a value from stack, that is random enough.
	float a = ((float*)argv)[-1];
	double b = 1.41;
	int c = (int)(a * b);
	atexit(afunc);
    printf("Hello World");
	FILE* h = fopen("a.txt", "wb");
	fprintf(h, "hello world %f, %f, %d \r\n", (double)a, b, c);
	int len = ftell(h);
	fseek(h, 0, SEEK_SET);
	char* inbuf = (char*)malloc(len + 1);
	fscanf(h,"%s", inbuf);
	fclose(h);
	free(inbuf);
	
	CallFromC();
	
	exit(0);
    return 0;
}