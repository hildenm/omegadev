#include <stdio.h>

class test
{
private:
	static const char* text;
	static test g_test;
	int m_v;
public:
	test(int v = 1);
	~test(void);
	void DoStuff(const test& t);
}; // test


const char* test::text = "Static text\r\n";
test test::g_test(3);

test::test(int v) : m_v(v)
{
}

test::~test(void)
{
}

void test::DoStuff(const test& t)
{
	g_test.m_v = (t.m_v + m_v);
	printf("%s%d\r\n", text, g_test.m_v);
}


void CallFromC(void)
{
	test* t1 = new(test);
	test t2 = test(2);

	for (int i = 0; i < 3; ++i)
	{
		t2.DoStuff(*t1);
	}
	
	delete t1;
}

