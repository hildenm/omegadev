
# Project build architecture settings
TOOLKIT	:= /usr/local/bin/m68k-elf

# Toolkit executables, libraries and directories settings
TOOLKIT_BIN	:= $(TOOLKIT)/bin
NEWLIB_LIB := $(TOOLKIT)/m68k-elf/lib
GCC_LIB := $(TOOLKIT)/lib/gcc/m68k-elf/13.2.0
TOOLKIT_INCLUDE := $(TOOLKIT)/include/atari_tos
CC := $(TOOLKIT_BIN)/m68k-elf-g++
AS := $(TOOLKIT_BIN)/m68k-elf-as
LD := $(TOOLKIT_BIN)/m68k-elf-g++
ELFTOPRG := $(TOOLKIT_BIN)/m68k-elf-prg

# Compiler and linker flags (be careful!)
CFLAGS := -I$(TOOLKIT_INCLUDE) -Wall -Os -g -m$(CPU) -msoft-float -flto
CXXFLAGS := -std=c++17 -fno-exceptions -fno-rtti
ASFLAGS := -g --register-prefix-optional -mcpu=$(CPU)
LDFLAGS := -nostdlib -L $(GCC_LIB) -L $(NEWLIB_LIB) -Xlinker --emit-relocs -Xlinker --no-warn-rwx-segments -static 

TARGET_PRG := $(BUILD_DIR)/$(TARGET_NAME)

# Defining target name
TARGET_ELF := $(BUILD_DIR)/$(basename $(TARGET_NAME))

# Building object list
OBJS := $(foreach source,$(SOURCES),$(BUILD_DIR)/$(basename $(source)).o)

# Make prg target
$(TARGET_PRG): $(TARGET_ELF)
	$(ELFTOPRG) $< $@

# Make elf target
$(TARGET_ELF): $(OBJS)
	$(LD) $(LDFLAGS) $^ -o $@ -Xlinker -T -Xlinker ldscripts/$(LINK_SCRIPT) $(LIBS)

# assembly
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.s
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) $(CXXFLAGS) -c $< -o $@

# Create build directory
$(BUILD_DIR): $(shell mkdir -p $(BUILD_DIR))

.PHONY: clean

all:	$(TARGET_PRG)

clean:
	$(shell rm -r $(BUILD_DIR))
