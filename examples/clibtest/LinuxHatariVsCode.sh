#!/bin/bash

# Set this to where your Hatari ST hard disk is.
export ST_HD_PATH=$HOME/sthd

# Set this to where your toolchain is. Most likely, just leave it as is.
export TOOLCHAIN_M68K=/usr/local/bin/m68k-elf

# These two lines are the virtual serial port pair that gdb and "gdbsrv.ttp" will use for communication.
export GDB_COM_PORT=$HOME/st_a
export SRV_COM_PORT=$HOME/st_b

# Leave this as is.
export PATH=$TOOLCHAIN_M68K:$PATH

# Make virtual serial ports using socat
socat PTY,link=$GDB_COM_PORT,raw,echo=0 PTY,link=$SRV_COM_PORT,raw,echo=0 & disown

# Start VsCode
code .

# Start Hatari with RS232 port set
hatari -d $ST_HD_PATH --gemdos-drive C --rs232-in $SRV_COM_PORT --rs232-out $SRV_COM_PORT
