/*
	We mainly want to test the functions that make use of anything contained in "omegadev/toolchain/libgloss"
	The whole idea of newlib is that if we provide a functioning libgloss, 
	everything else in libc	should be built OK.
	
	stat and fstat is tested several times during fprintf etc.
*/

#include <stdio.h>
#include <traps.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

/*
	We don't get settimeofday declared in sys/time.h, as we don't have correct settings...
	Can we fix that when building newlib?
*/
#ifndef settimeofday
#ifdef __cplusplus
extern "C" {
#endif
int settimeofday(const struct timeval* tv, const struct timezone* tz);
#ifdef __cplusplus
}
#endif
#endif

const char testDir1[] = "C:\\TESTDIR";
const char testFileName1[] = "fileio1.bin";
const char testFileName2[] = "fileio2.bin";
const char testFileName3[] = "D:\\fileio3.bin";
const char sixteenSpaces[] = "                ";
char tmpString[256];
unsigned int tmpNum;

// File IO tests
// If these works, then a LOT of other functions have been tested too.
int FileIO(void)
{
	int result = 0;
	FILE* f = 0;
	int ret;
	unsigned int rememberFirstHandle;
	// First file open
	if ((f = fopen(testFileName1, "w")) == 0)
	{
		printf("FileIO: fopen \"w\" failed.\r\n");
		perror(0);
		return -1;
	}
	if ((ret = fwrite(sixteenSpaces, 1, 16, f)) != 16)
	{
		printf("FileIO: fwrite sixteenSpaces failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = ftell(f)) != 16)
	{
		printf("FileIO: ftell 16 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fseek(f, -4, SEEK_CUR)) != 0)
	{
		printf("FileIO: fseek CUR failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = ftell(f)) != 12)
	{
		printf("FileIO: ftell 12 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fseek(f, 0, SEEK_END)) != 0)
	{
		printf("FileIO: fseek END 1 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = feof(f)) != 0)
	{
		printf("FileIO: feof failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = ftell(f)) != 16)
	{
		printf("FileIO: ftell 16 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	rememberFirstHandle = (unsigned int)f;
	if ((ret = fprintf(f, "FileHandle %x\r\n", rememberFirstHandle)) <= 0)
	{
		printf("FileIO: fprintf failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fflush(f)) != 0)
	{
		printf("FileIO: fflush failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	int curFileSize1 = ftell(f);
	if (curFileSize1 < 0)
	{
		printf("FileIO: ftell curFileSize1 failed: %d\r\n", curFileSize1);
		perror(0);
		result = -1;
	}
	if ((ret = fseek(f, 0, SEEK_SET)) != 0)
	{
		printf("FileIO: fseek SET 0 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fread(tmpString, 1, 8, f)) != 0)
	{
		printf("FileIO: fread write only failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose \"w\" failed: %d\r\n", ret);
		perror(0);
		return -1;
	}

	// Second file open
	if ((f = fopen(testFileName1, "r")) == 0)
	{
		printf("FileIO: fopen \"r\" failed.\r\n");
		perror(0);
		return -1;
	}
	if ((ret = fseek(f, 8, SEEK_SET)) != 0)
	{
		printf("FileIO: fseek SET failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fread(tmpString, 1, 8, f)) != 8)
	{
		printf("FileIO: fread 8 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fscanf(f, "%s%x", tmpString, &tmpNum)) != 2)
	{
		printf("FileIO: fscanf failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if (tmpNum != rememberFirstHandle)
	{
		printf("FileIO: fscanf number is not same: %x, %x\r\n", tmpNum, rememberFirstHandle);
		perror(0);
		result = -1;
	}
	if ((ret = fwrite(sixteenSpaces, 1, 16, f)) != 0)
	{
		printf("FileIO: fwrite read only failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fseek(f, 0, SEEK_END)) != 0)
	{
		printf("FileIO: fseek END 2 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = feof(f)) != 0)
	{
		printf("FileIO: feof failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	int curFileSize2 = ftell(f);
	if (curFileSize2 != curFileSize1)
	{
		printf("FileIO: ftell curFileSize failed: %d, %d\r\n", curFileSize1, curFileSize2);
		perror(0);
		result = -1;
	}
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose \"r\" failed: %d\r\n", ret);
		perror(0);
		return -1;
	}

	// Third file open
	if ((f = fopen(testFileName1, "a+")) == 0)
	{
		printf("FileIO: fopen \"a+\" failed.\r\n");
		perror(0);
		return -1;
	}
	if ((ret = fseek(f, 0, SEEK_SET)) != 0)
	{
		printf("FileIO: fseek SET failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fread(tmpString, 1, 8, f)) != 8)
	{
		printf("FileIO: fread 8 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fwrite(sixteenSpaces, 1, 11, f)) != 11)
	{
		printf("FileIO: fwrite eleven spaces failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	int curFileSize3 = ftell(f);
	if (curFileSize3 != (curFileSize2 + 11))
	{
		printf("FileIO: ftell after append failed: %d, %d\r\n", curFileSize3, curFileSize2);
		perror(0);
		result = -1;
	}
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose \"a+\" failed: %d\r\n", ret);
		perror(0);
		return -1;
	}

	// Fourth file open
	if ((f = fopen(testFileName1, "a")) == 0)
	{
		printf("FileIO: fopen \"a\" failed.\r\n");
		perror(0);
		return -1;
	}
	if ((ret = fread(tmpString, 1, 8, f)) != 0)
	{
		printf("FileIO: fread write only failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fwrite(sixteenSpaces, 1, 11, f)) != 11)
	{
		printf("FileIO: fwrite eleven spaces failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	int curFileSize4 = ftell(f);
	if (curFileSize4 != (curFileSize3 + 11))
	{
		printf("FileIO: ftell after append failed: %d, %d\r\n", curFileSize4, curFileSize3);
		perror(0);
		result = -1;
	}
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose \"a\" failed: %d\r\n", ret);
		perror(0);
		return -1;
	}

	if (rename(testFileName1, testFileName2) != 0)
	{
		printf("FileIO: rename failed.\r\n");
		perror(0);
		result = -1;
	}

	// Fifth file open
	if ((f = fopen(testFileName2, "r+")) == 0)
	{
		printf("FileIO: fopen \"r+\" failed.\r\n");
		perror(0);
		return -1;
	}
	if ((ret = fread(tmpString, 1, 8, f)) != 8)
	{
		printf("FileIO: fread write only failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fseek(f, 0, SEEK_CUR)) != 0)
	{
		printf("FileIO: fseek CUR failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fwrite(sixteenSpaces, 1, 11, f)) != 11)
	{
		printf("FileIO: fwrite eleven spaces failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fseek(f, 0, SEEK_END)) != 0)
	{
		printf("FileIO: fseek END 3 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = ftell(f)) != curFileSize4)
	{
		printf("FileIO: ftell after write failed: %d, %d\r\n", ret, curFileSize4);
		perror(0);
		result = -1;
	}
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose \"r+\" failed: %d\r\n", ret);
		perror(0);
		return -1;
	}

	// Sixth file open
	if ((f = fopen(testFileName2, "w+")) == 0)
	{
		printf("FileIO: fopen \"w+\" failed.\r\n");
		perror(0);
		return -1;
	}
	if ((ret = fwrite(sixteenSpaces, 1, 16, f)) != 16)
	{
		printf("FileIO: fwrite sixteen spaces failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fseek(f, 0, SEEK_SET)) != 0)
	{
		printf("FileIO: fseek SET failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fread(tmpString, 1, 16, f)) != 16)
	{
		printf("FileIO: fread failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = feof(f)) != 0)
	{
		printf("FileIO: feof failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose \"w+\" failed: %d\r\n", ret);
		perror(0);
		return -1;
	}

	if (remove(testFileName2) != 0)
	{
		printf("FileIO: remove failed.\r\n");
		perror(0);
		result = -1;
	}

	// Test all open scenarios that should fail if file don't exist.
	if ((f = fopen(testFileName2, "r")) != 0)
	{
		printf("FileIO: fopen \"r\" failed to fail.\r\n");
		perror(0);
		fclose(f);
		result = -1;
	}
	if ((f = fopen(testFileName2, "r+")) != 0)
	{
		printf("FileIO: fopen \"r+\" failed to fail.\r\n");
		perror(0);
		fclose(f);
		result = -1;
	}

/*
	// This test works.
	// But the reconnection of stdout is undefined, and for newlib it simply drops stdout after reconnection.
	// So we cannot see any printouts after this test, meaning that I chose not to run it.
	
	// Try redirecting streams
	if ((f = freopen(testFileName1, "w+", stdout)) == 0)
	{
		printf("FileIO: freopen \"w+\" failed.\r\n");
		return -1;
	}
	int written = printf("This is redirected to a file.");
	int flength = ftell(f);
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose from freopen \"w+\" failed: %d\r\n", ret);
		return -1;
	}
	if (flength != written)
	{
		printf("FileIO: ftell after printf failed: %d, %d\r\n", flength, written);
	}
*/	

	if ((ret = mkdir(testDir1, 0)) != 0)
	{
		printf("FileIO: mkdir failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = chdir(testDir1)) != 0)
	{
		printf("FileIO: chdir 1 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	char* tmpBuf;
	if ((tmpBuf = getcwd(tmpString, 256)) == 0)
	{
		printf("FileIO: getcwd 1 failed.\r\n");
		perror(0);
		result = -1;
	}
	else if (strcmp(tmpBuf, testDir1) != 0)
	{
		printf("FileIO: getcwd 1 failed: %s\r\n", tmpBuf);
		perror(0);
		result = -1;
	}
	if ((ret = chdir("..")) != 0)
	{
		printf("FileIO: chdir 2 failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((tmpBuf = getcwd(0, 128)) != 0)
	{
		if (strcmp(tmpBuf, "C:") != 0)
		{
			printf("FileIO: getcwd 2 failed: %s\r\n", tmpBuf);
			result = -1;
		}
		free(tmpBuf);
	}
	else
	{
		printf("FileIO: getcwd 2 failed.\r\n");
		perror(0);
		result = -1;
	}
	if ((ret = rmdir(testDir1)) != 0)
	{
		printf("FileIO: rmdir failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}

	if ((f = fopen(testFileName3, "w")) == 0)
	{
		printf("FileIO: fopen on D: failed.\r\n");
		perror(0);
		return -1;
	}
	if ((ret = fprintf(f, "Writing on D:\r\n")) <= 0)
	{
		printf("FileIO: fprintf on D: failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	if ((ret = fclose(f)) != 0)
	{
		printf("FileIO: fclose on D: failed: %d\r\n", ret);
		perror(0);
		return -1;
	}
	
	return result;
}

int Time(void)
{
	int result = 0;
	int ret;
	time_t ctime = time(NULL);
	struct tm t = *localtime(&ctime);
	/*
	t.tm_year = 123;	// since 1900
	t.tm_mon = ((date >> 5) & 0xf) - 1;
	t.tm_mday = date & 0x1f;
	t.tm_hour = ((time >> 11) & 0x1f);
	t.tm_min = ((time >> 5) & 0x3f);
	t.tm_sec = (time & 0x1f) * 2;
	t.tm_isdst = -1;
	*/
	//mktime(&t);
	printf("Today is: %s\r\n", asctime(&t));
	
	time_t seconds1 = time(NULL);
	// wait for change
	int changed = -1;
	for (int i = 0; i < 1000000; ++i)
	{
		if (time(NULL) != seconds1)
		{
			changed = 0;
			break;
		}
	}
	if (changed != 0)
	{
		printf("Time: time doesn't pass.\r\n");
		return -1;
	}
	struct timeval tv1, tv2;
	gettimeofday(&tv1, 0);
	if ((ret = settimeofday(&tv1, 0)) < 0)
	{
		printf("Time: settimeofday failed: %d\r\n", ret);
		perror(0);
		result = -1;
	}
	gettimeofday(&tv2, 0);
	double diff = difftime(tv1.tv_sec, tv2.tv_sec);
	if (diff < -2 || diff > 2)
	{
		printf("Time: difftime fail: %d\r\n", (int)diff);
		result = -1;
	}
	return result;
}
  

int main(int argc, char** argv)
{
	for (int i = 0; i < argc; ++i)
	{
		printf("argv[%d]: %s\r\n", i, argv[i]);
	}
	int result = -1;
	printf("Starting tests.\r\n");
	do
	{
		if (FileIO() != 0) {break;}
		if (Time() != 0) {break;}
		result = 0;
	} while (false);
	if (result == 0)
	{
		printf("All tests OK.\r\n");
	}
	else
	{
		printf("Tests failed!\r\n");
	}
	printf("Press any key.\r\n");
	while (Bconstat(DEV_CON) == 0)
	{
	}
	return 0;
}