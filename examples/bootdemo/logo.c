#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*
    squares[4] {
        x,y,
        x,y,
        x,y,
        x,y
    }
*/

char* chars[] = {"O","M","E","G","A"};
short logo[] = {
    // Character 'O'
    // square one
    -10, -2,
    -7, -2,
    -8, -1,
    -9, -1,
    // square two
    -10, -2,
    -9, -1,
    -9, 2,
    -10, 3,
    // square three
    -7, -2,
    -7, 3,
    -8, 2,
    -8, -1,
    // square four
    -9, 2,
    -8, 2,
    -7, 3,
    -10, 3,
 
    // Character 'M'
    // square one
    -6, -2,
    -5, -2,
    -5, 3,
    -6, 3,
    // square two
    -3, -2,
    -2, -2,
    -2, 3,
    -3, 3,
    // square three
    -6, -2,
    -5, -2,
    -4, -1,
    -4, 1,
    // square four
    -3, -2,
    -2, -2,
    -4, 1,
    -4, -1,

    // Character 'E'
    // square one
    -1, -2,
    0, -1,
    0, 2,
    -1, 3,
    // square two
    -1, -2,
    2, -2,
    2, -1,
    0, -1,
    // square three
    0, 0,
    2, 0,
    2, 1,
    0, 1,
    // square four
    0, 2,
    2, 2,
    2, 3,
    -1, 3,

    // Character 'G'
    // square one
    3, -2,
    6, -2,
    6, -1,
    4, -1,
    // square two
    3, -2,
    4, -1,
    4, 2,
    3, 3,
    // square three
    4, 2,
    5, 2,
    6, 3,
    3, 3,
    // square four
    5, 1,
    6, 1,
    6, 3,
    5, 2,

    // Character 'A'
    // square one
    8, -2,
    9, -2,
    8, 3,
    7, 3,
    // square two
    8, -2,
    9, -2,
    10, 3,
    9, 3,
    // square three
    7, 1,
    10, 1,
    10, 2,
    7, 2
};

/*
    We want a cos/sin combination with low error tolerance.
*/
void FindTheta(FILE* fd)
{
    fprintf(fd, "\n| Help information for choosing rotational values.\n");
    fprintf(fd, "| The error is for the cosine value.\n\n");
    for (int sin_theta = 1; sin_theta < 0x4000; ++sin_theta)    // 45 degrees
    {
        int square = (0x8000*0x8000)-(sin_theta*sin_theta);
        double root = sqrt((double)square);
        int cos_theta = (int)(root + 0.5);
        if (cos_theta == 0x8000) {continue;}
        double error = fabs(root - (double)cos_theta);
        if (error < 0.0005) // only output small errors.
        {
            double degree = asin((double)sin_theta / 32768.0) * (180.0 / 3.141592654);
            fprintf(fd, "| error: %f, degree %f\n", error, degree);
            fprintf(fd, "|\tcos: 0x%x, sin 0x%x\n", cos_theta, sin_theta);
        }
    }
}

int main(int argc, char *argv[])
{
	int result = -1;
	if (argc != 2)
	{
		printf("Not correct number of arguments!\n");
		return -1;
	}
    char filename[1024];
    sprintf(filename, "%s/logo.s", argv[1]);

	FILE* fd = NULL;
	do
	{
		if ((fd = fopen(filename, "wb")) == NULL)
		{
			printf("Could not open logo.s\n");
			break;
		}	
    	// write output
        fprintf(fd, "| Each square is one long word.\n");
        fprintf(fd, "| LSB is x offset.\n");
        fprintf(fd, "| Coordinates is top 24 bits:\n");
        fprintf(fd, "|\t4[3 bits x, 3 bits y]\n");

        fprintf(fd, "\nlogo:\n");
        int num_squares = 4 * 4 + 3; // A is only three
        for (int s = 0; s < num_squares; ++s)
        {
            int cpos = s * 8;
            if ((s & 3) == 0)
            {
                fprintf(fd, "| \"%s\":\n", chars[s>>2]);
            }
            short tmp[16];
            // Each square coordinates is sorted so that coordinate 0 is top left.
            short x = 1000;
            short tx = 1000;
            short ty = 1000;
            int topleft = 0;
            for (int coord = 0; coord < 4; coord++)
            {
                tmp[coord*2 + 0] = logo[cpos];
                tmp[coord*2 + 1] = logo[cpos + 1];
                tmp[coord*2 + 8] = logo[cpos];
                tmp[coord*2 + 9] = logo[cpos + 1];
                if (logo[cpos] < x)
                {
                    x = logo[cpos];
                }
                if ((logo[cpos + 1] < ty) || (logo[cpos + 1] == ty && logo[cpos] < tx))
                {
                    ty = logo[cpos + 1];
                    tx = logo[cpos];
                    topleft = coord;
                }
                cpos += 2;
            }
            // Top three bits is always 0... A bit of a waste...
            unsigned int packed = 0;
            for (int coord = 0; coord < 4; coord++)
            {
                int tpos = (coord + topleft) * 2;
                /*
                packed = packed << 6;
                packed |= ((tmp[tpos] - x)<<3) | (tmp[tpos + 1] + 2);
                */
                packed = packed << 8;
                packed |= ((tmp[tpos]&0x1f)<<3) | (tmp[tpos + 1]&0x7);
            }
            /*
            // x is +-10, which is 5 bits...
            packed = packed << 8;
            packed |= x&0xff;
            */
            fprintf(fd, "\t.dc.l\t0x%x\n", packed);
        }
		/*
            Add some extra useful stuff
        */
        FindTheta(fd);
		// And we are done!
		result = 0;
	} while(0);

	if (fd != NULL)
	{
		fclose(fd);
		fd = NULL;
	}		

	return result;
}