/*
	Needless to say:
	Speed and accuracy was sacrificed in favor of size.
*/

	| See build/logo.s and logo.c for information about rotational values. 
	| 3.581570 degree step
	.equ	cos_theta, 0x7fc0
	.equ	sin_theta, 0x7ff
	.equ	eye, 20 << 6
	.equ	eye2, eye * 12

	.text
.ifdef BOOT_SECTOR
	| Bootsector checksum calculated by bootfix.c
	.global _start
_start:
.else
	| Code for running as a prg so we can easily test and debug.
	.global main
main:
	/*
		Need low resolution and supervisor mode.
		Also need to reset those when returning to desktop.
	*/
	lea	super(pc), a0
	move.l	a0, -(a7)
	move.w	#0x26, -(a7)
	trap	#14
	addq.l	#6, a7
	rts
super:
	move.b	0xffff8260.w, -(a7)
	| Wait for vbl to safely change resolution
	moveq	#0, d0
	lea		0xffff8205.w, a0
1:
	move.w	d0, d1
	movep.w	0(a0), d0
	cmp.w	d1, d0
	jcc		1b
	clr.b	0xffff8260.w
	jbsr run_boot_code
	move.b	(a7)+, 0xffff8260.w
	moveq	#0, d0
	rts
.endif
run_boot_code:
	lea		0xffff8203.w, a5
	move.b	0xffff8201.w, -(a7)
	bra.s	after_disk_params
	.byte	0x00		| last byte of volume serial number. *high byte checksum!
	.byte	0x00, 0x02	| bps
	.byte	0x02		| spc
	.byte	0x01, 0x00	| ressec
	.byte	0x02		| nfats
	.byte	0x10, 0x00	| ndirs
	.byte	0xa0, 0x05	| nsects
	.byte	0xf8		| media *This byte is not used on floppies = low byte checksum!*
	.byte	0x02, 0x00	| spf
	.byte	0x09, 0x00	| spt
	.byte	0x02, 0x00	| heads
	.byte	0x00, 0x00	| nhid
	
after_disk_params:
	/*
		We are always in low res for st/ste.
		Always in supervisor.
	*/
	move.b	(a5), -(a7)
	move.l	0xffff8240.w, -(a7)
| init vbl main	
| Set screenbuffers to 0x40000 and 0x48000
	clr.b	(a5)
wait_for_vbl:	
	moveq	#0x4, d0
	move.b	d0, 0xffff8201.w
	swap	d0
	move.b	(a5), d0
	eor.b	#0x80, (a5)
	move.b	(a5), d1
1:
	cmp.b	0xffff8207.w, d1
	jne		1b
	lsl.w	#8, d0
main_loop:
	| Clear screen
	move.l	d0, a2
	move.w	#7999, d1
1:
	clr.l	(a2)+
	dbra	d1, 1b

	jbsr	draw_logo
	| check space key
	cmp.b	#0xb9, 0xfffffc02.w
	jne		wait_for_vbl
	move.l	(a7)+, 0xffff8240.w
	move.b	(a7)+, (a5)
	move.b	(a7)+, 0xffff8201.w
	rts

draw_logo:
	move.l	d0, a2

| Update rotational matrix
	lea	matrix(pc), a4
	movem.w	(a4), d0/d1
	movem.w	(a4), d2/d3
	move.w	#cos_theta, d4
	move.w	#sin_theta, d5

	muls	d4, d0
	muls	d5, d1
	add.l	d1, d0
	add.l	d0, d0
	swap	d0

	muls	d4, d3
	muls	d5, d2
	sub.l	d2, d3
	add.l	d3, d3
	swap	d3
	movem.w	d0/d3, (a4)

| Unpack coordinates and draw logo
	lea		logo(pc), a0
	| 19 squares
	moveq	#18, d2
nextSquare:
	| Four coordinates per square
	lea 	32000(a2), a3
	moveq	#-1, d7
	moveq	#3, d5
3:
	move.b	(a0), d3
	ext.w	d3
	asr.w	#3, d3
	| Rotate x and z
	move.w	d3, d4
	muls	(a4), d3		| x = +-10 << 14
	muls	2(a4), d4		| z = +-10 << 14
	asr.l	#8, d4			| z = +-10 << 6
	move.w	#eye2, d6
	add.w	#eye, d4
	| Calc z perspective
	| Adjust x perspective and center
	lsl.l	#2, d3

	divs	d4, d3
	muls	d6, d3
	clr.w	d3
	add.l	#(160<<16)+100, d3

	move.b	(a0)+, d0
	lsl.b	#5, d0
	ext.w	d0
	asr.w	#5, d0

	| Adjust y perspective and center
	muls	d6, d0
	divs	d4, d0
	add.w	d0, d3

	cmp.w	d7, d3
	jcs		4f
	jne		2f
	cmp.l	d7, d3
	jcc		2f
4:
	move.l	d3, d7
	move.l	a3, a1
2:
	move.l	d3, 16(a3)
	move.l	d3, (a3)+

	dbra	d5, 3b

	movem.l	d2/a0/a2/a4/a5, -(a7)
/*
	a1 = coordinates
	a2 = screen address
	a4 = matrix
*/
draw_square:

| Make stack space and clear counters
	moveq	#0, d5
	moveq	#0, d6
| First check that coordinates are in clockwise order.
	movem.l	(a1),d0-d3
	tst.w	(a4)
	jpl		1f
	exg		d1, d3
1:
	movem.l	d0-d3, (a1)
	lea		16(a1), a0

	mulu	#160, d0
	lea		(a2, d0.w), a2

next_coord:
| Find out if left and/or right side needs to be calculated
	cmp.l	a0, a1
	jcs		continue_draw
| done, return stack space.
end_draw_square:	
	movem.l		(a7)+, d2/a0/a2/a4/a5

	dbra	d2, nextSquare
	| We take advantage of the fact that d2 will be 0x0000ffff here, to set the colors.
	move.l	d2, 0xffff8240.w
	rts

calc_slope:
	moveq	#0x40, d3
	lsl.w	#8, d3
	divu	d2, d3
	muls	d4, d3
	lsl.l	#2, d3
	rts

continue_draw:
	tst.w	d5
	jne		left_slope_not_finished

	move.w	(a0), d0
	move.w	-(a0), d2
	move.w	-(a0), d4
	sub.w	6(a0), d2
	jeq		next_coord
	sub.w	d0, d4
	swap	d0
	clr.w	d0
	move.w	d2, d5
	jbsr	calc_slope
	move.l	d3, a4
left_slope_not_finished:

	tst.w	d6
	jne		right_slope_not_finished

	move.w	(a1)+, d1
	move.w	4(a1), d2
	sub.w	(a1)+, d2
	jeq		next_coord
	move.w	(a1), d4
	sub.w	d1, d4
	swap	d1
	clr.w	d1
	move.w	d2, d6
	jbsr	calc_slope
	move.l	d3, a3
right_slope_not_finished:

	move.w	d6, d7
	cmp.w	d5, d7
	jcs		1f
	move.w	d5, d7
1:
	sub.w	d7, d5
	sub.w	d7, d6
/*
	input:
	a4 = 14:16 left delta
	a3 = 14:16 right delta
	a2 = screen line address
	d0 = 14:16 left pixel pos
	d1 = 14:16 right pixel pos
	d7 = line count to next coord
	
	uses:
	d2-d4
	a5-a6
*/
h_line:
	move.l	d1, d2
	jbsr	calc_edge
	move.l	a5, a6
	move.w	d2, d3
	not.w	d3

	move.l	d0, d2
	jbsr	calc_edge

	jra		2f
1:
	or.w	d2, (a5)
	moveq	#-1, d2
	addq.l	#8, a5
2:
	cmpa.l	a6, a5
	jcs		1b
	and.w	d2, d3
	or.w	d3, (a6)

	add.l	a4, d0
	add.l	a3, d1
	lea		160(a2), a2
	subq.w	#1, d7
	jne		h_line
	jra next_coord

calc_edge:
	swap	d2
	move.w	d2, d4
	and.w	#0xf, d4
	sub.w	d4, d2
	lsr.w	#1, d2
	lea		(a2, d2.w), a5
	moveq	#-1, d2
	lsr.w	d4, d2
	rts

	.include "build/logo.s"		| Created by logo.c
matrix:
	.dc.w	0x4000, 0x0000
