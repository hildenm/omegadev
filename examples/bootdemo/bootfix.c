#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int result = -1;
	if (argc != 3)
	{
		printf("Not correct number of arguments!\n");
		return -1;
	}
	char* source = argv[1];
	char* dest = argv[2];
	char disk[1024];
	sprintf(disk, "%s.st", dest);
	
	unsigned char infile[514];
	FILE* fs = NULL;
	FILE* fd = NULL;
	FILE* fdsk = NULL;
	do
	{
		if ((fs = fopen(source, "rb")) == NULL)
		{
			printf("Could not open %s\n", source);
			break;
		}
		if ((fd = fopen(dest, "wb")) == NULL)
		{
			printf("Could not open %s\n", dest);
			break;
		}	
		if ((fdsk = fopen(disk, "wb")) == NULL)
		{
			printf("Could not open %s\n", disk);
			break;
		}	
		int len = fread(infile, 1, 513, fs);
		if (len > 512 || len < 0)
		{
			printf("Bad size input file.\n");
			break;
		}
		// Pad with zeroes.
		for (int i = len; i < 512; ++i) {infile[i] = 0;}
		// Calc checksum
		infile[10] = 0;
		infile[21] = 0;

		unsigned short checksum = 0;
		for (int i = 0; i < 256; ++i)
		{
			unsigned short v = (((unsigned short)infile[(i * 2) + 0]) << 8) | ((unsigned short)infile[(i * 2) + 1]);
			checksum += v;
		}
		checksum = 0x1234 - checksum;
		infile[10] = (unsigned char)(checksum >> 8);
		infile[21] = (unsigned char)checksum;

		// write output
		fwrite(infile, 1, 512, fd);
		
		// write st disk image
		fwrite(infile, 1, 512, fdsk);
		// Fill zeroes
		memset(infile, 0, 512);
		for (int i = 0; i < (80*9*2) - 1; ++i)
		{
			fwrite(infile, 1, 512, fdsk);
		}

		// And we are done!
		result = 0;
	} while(0);

	if (fs != NULL)
	{
		fclose(fs);
		fs = NULL;
	}		
	if (fd != NULL)
	{
		fclose(fd);
		fd = NULL;
	}		
	if (fdsk != NULL)
	{
		fclose(fdsk);
		fdsk = NULL;
	}		

	return result;
}