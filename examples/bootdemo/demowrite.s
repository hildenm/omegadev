/*
    Reads current boot sector and copies the disk information bytes to the new.
    Calculates a bootable checksum and then save the new boot sector.

    According to Atari specifications, checksum is stored at byte 510, but that is not necessary.
    It is only necessary that all the words added together becomes 0x1234.
    So to save two bytes of memory, the checksum is split in two bytes
    and saved at places otherwise unused. 
*/
	.text

    .equ    checksum_low, 21    | must be odd
    .equ    checksum_high, 10   | must be even

	.global main
main:
    | Read current boot sector
    moveq   #0, d0      | read
    lea     oldboot(pc), a0
    jbsr    read_write

    | Copy disk format bytes to new boot sector
    lea     oldboot + 11(pc), a0
    lea     bootsector + 11(pc), a1
    moveq   #18, d0
1:
    move.b  (a0)+, (a1)+
    dbra    d0, 1b

    | Calc checksum
    lea     bootsector(pc), a0
    clr.b   checksum_high(a0)
    clr.b   checksum_low(a0)
    moveq   #0, d1
    move.w  #255, d0
2:
    add.w   (a0)+, d1
    dbra    d0, 2b

    move.w  #0x1234, d0
    sub.w   d1, d0

    | Store checksum
    lea     bootsector(pc), a0
    move.b  d0, checksum_low(a0)
    lsr.w   #8, d0
    move.b  d0, checksum_high(a0)

    | Write boot demo
    moveq   #1, d0      | write
    lea     bootsector(pc), a0
    jbsr    read_write
    |moveq   #0, d0
    rts

read_write:
    move.l  #0,-(a7)
    move.w  #0,-(a7)
    move.w  #0,-(a7)
    move.w  #1,-(a7)
    move.l  a0,-(a7)
    move.w  d0,-(a7)
    move.w  #4,-(a7)
    trap    #13
    lea     18(a7),a7
    rts

    .data
bootsector:
    .incbin "build/demo.boot"
    .bss
    .lcomm  oldboot, 512
