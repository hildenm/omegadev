.DEFAULT_GOAL := all

# Project source and destination settings
TARGET_NAME := demo
BUILD_DIR := build
SRC_DIR := .
CPU := 68000

LINK_SCRIPT := atari-st-micro.ld		# C/C++/ASM no library functions at all, and no init/exit of global classes. Small executable file, maybe good for demos.

# Project build architecture settings
TOOLKIT	:= /usr/local/bin/m68k-elf

# Toolkit executables, libraries and directories settings
TOOLKIT_BIN	:= $(TOOLKIT)/bin
NEWLIB_LIB := $(TOOLKIT)/m68k-elf/lib
GCC_LIB := $(TOOLKIT)/lib/gcc/m68k-elf/13.2.0
TOOLKIT_INCLUDE := $(TOOLKIT)/include/atari_tos
AS := $(TOOLKIT_BIN)/m68k-elf-as
LD := $(TOOLKIT_BIN)/m68k-elf-g++
ELFTOPRG := $(TOOLKIT_BIN)/m68k-elf-prg

# Compiler and linker flags (be careful!)
ASFLAGS := -g --register-prefix-optional -mcpu=$(CPU)
LDFLAGS := -nostdlib -L $(GCC_LIB) -L $(NEWLIB_LIB) -Xlinker --emit-relocs -Xlinker --no-warn-rwx-segments -static 
BOOTLDFLAGS := -nostdlib  -Xlinker --oformat=binary -Xlinker --no-warn-rwx-segments -static 

# Targets
TARGET_PRG := $(BUILD_DIR)/$(TARGET_NAME).prg
TARGET_ELF := $(BUILD_DIR)/$(TARGET_NAME)
TARGET_BIN := $(BUILD_DIR)/$(TARGET_NAME).bin
TARGET_BOOT := $(BUILD_DIR)/$(TARGET_NAME).boot
WRITER_PRG := $(BUILD_DIR)/$(TARGET_NAME)wrte.prg
WRITER_ELF := $(BUILD_DIR)/$(TARGET_NAME)wrte

# Build bootfix
$(BUILD_DIR)/bootfix: bootfix.c
	gcc $< -o $@

# Build logo
$(BUILD_DIR)/logo: logo.c
	gcc $< -o $@ -lm

$(BUILD_DIR)/logo.s: $(BUILD_DIR)/logo
	$(BUILD_DIR)/logo $(BUILD_DIR)

main.s: $(BUILD_DIR)/logo.s  

demowrite.s: $(TARGET_BOOT)

# Assemble
$(BUILD_DIR)/main.o: main.s
	$(AS) $(ASFLAGS) -c $< -o $@

$(BUILD_DIR)/demowrite.o: demowrite.s
	$(AS) $(ASFLAGS) -c $< -o $@

$(BUILD_DIR)/boot.o: main.s
	$(AS) $(ASFLAGS) -defsym BOOT_SECTOR= -c $< -o $@

# Build targets
$(TARGET_PRG): $(TARGET_ELF)
	$(ELFTOPRG) $< $@

$(TARGET_ELF): $(BUILD_DIR)/main.o
	$(LD) $(LDFLAGS) $^ -o $@ -Xlinker -T -Xlinker ldscripts/$(LINK_SCRIPT) $(LIBS)

$(WRITER_PRG): $(WRITER_ELF)
	$(ELFTOPRG) $< $@

$(WRITER_ELF): $(BUILD_DIR)/demowrite.o
	$(LD) $(LDFLAGS) $^ -o $@ -Xlinker -T -Xlinker ldscripts/$(LINK_SCRIPT) $(LIBS)

$(TARGET_BIN): $(BUILD_DIR)/boot.o
	$(LD) $(BOOTLDFLAGS) $^ -o $@

$(TARGET_BOOT): $(TARGET_BIN) $(BUILD_DIR)/bootfix
	$(BUILD_DIR)/bootfix $(TARGET_BIN) $(TARGET_BOOT)

# Create build directory
$(BUILD_DIR): $(shell mkdir -p $(BUILD_DIR))

all:	$(BUILD_DIR) $(BUILD_DIR)/logo $(TARGET_PRG) $(TARGET_BOOT) $(WRITER_PRG)

clean:
	$(shell rm -r $(BUILD_DIR))


