/*
	Example code used for testing gdbserver.
*/
#include <traps.h>

volatile short * const palette = (short * const)0xffff8240;

int SuperMain(void)
{
	while (true)
	{
		palette[0] += 1;
	}
	return 0;
}

int main(int argc, char** argv)
{
	return Supexec(SuperMain);
}