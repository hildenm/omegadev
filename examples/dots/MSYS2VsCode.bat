rem Set this to where your ST hard disk is. Real or emulator.
set ST_HD_PATH=C:\test\sthd
rem Set this to where your toolchain is. Most likely, just leave it as is.
set TOOLCHAIN_M68K=C:\msys64\usr\local\bin\m68k-elf
rem set this to the serial port GDB should communicate on. If using virtual com ports,
rem then set this to the port that is NOT used by emulator.
set GDB_COM_PORT=COM5

rem Leave this as is.
set PATH=C:\msys64\ucrt64\bin;C:\msys64\usr\bin;%TOOLCHAIN_M68K%;%PATH%
code .
