
	.text
	
	.global sinus

wavesInit:
	movem.l	d0-d3/a0-a2, -(a7)
	lea	sinus, a0
	lea sin_x, a1
	lea sin_y, a2
	move.w	#0x7fff, d0
1:
	move.w	(a0)+, d1
	move.w	d1, d2
	muls	#79 * 4, d1
	muls	#49 * 4, d2
	swap	d1
	swap	d2
	add.w	#80, d1
	add.w	#50, d2
	mulu	#160, d2
	move.w	d1, (a1)+
	move.w	d2, (a2)+
	dbra	d0, 1b
	movem.l	(a7)+, d0-d3/a0-a2
	rts
	
	.global asm_Init
asm_Init:
	.func 	asm_Init
	
	bsr		wavesInit
	
	move.w	#0x2700, sr
	move.l	0x70.w, vblVec
	move.l	#vbl, 0x70.w
	move.w	#0x2300, sr
	rts
	.endfunc

	.global asm_Exit
asm_Exit:
	.func 	asm_Exit
	move.w	#0x2700, sr
	move.l	vblVec, 0x70.w
	move.w	#0x2300, sr
	rts
	.endfunc

	.global asm_WaitVbl
asm_WaitVbl:
	.func 	asm_WaitVbl
	tst.b	vblflag
	beq.s	asm_WaitVbl
	sf		vblflag
	
	moveq	#0, d0	| return false
	cmp.b	#0x39,0xfffffc02.w	| space
	bne.s	1f
2:
	cmp.b	#0xb9,0xfffffc02.w	| space+$80
	bne.s	2b
	moveq	#1, d0	| return true
1:
	rts
	.endfunc


	.global asm_FastClearScreen
asm_FastClearScreen:
	.func 	asm_FastClearScreen
	movem.l	d0-d7/a0-a3, -(a7)
/*
	Frame address is pushed to stack before calling this subroutine.
	So to get it we need to add the offset of the bytes we just pushed,
	and also add the bytes pushed by the jsr that called us.
	So, twelve long words + a long word return address is 52 bytes.
*/
	move.l	52(a7), a0
	moveq	#0, d1
	move.l	d1, d2
	move.l	d1, d3
	move.l	d1, d4
	move.l	d1, d5
	move.l	d1, d6
	move.l	d1, d7
	move.l	d1, a1
	move.l	d1, a2
	move.l	d1, a3
	moveq	#79, d0
1:
	offset=0
	.rept	10
	movem.l	d1-d7/a1-a3, offset(a0)
	offset=offset+40
	.endr
	lea		offset(a0), a0
	dbra	d0, 1b

	movem.l	(a7)+, d0-d7/a0-a3
	rts
	.endfunc

	.global asm_FastClearScreen_1plane
asm_FastClearScreen_1plane:
	.func 	asm_FastClearScreen_1plane
	movem.l	d0-d1/a0, -(a7)
	move.l	16(a7), a0
	moveq	#0, d1
	moveq	#99, d0
1:
	offset=0
	move.w	d1, (a0)
	.rept	39
	offset=offset+8
	move.w	d1, offset(a0)
	.endr
	lea		320(a0), a0
	dbra	d0, 1b

	movem.l	(a7)+, d0-d1/a0
	rts
	.endfunc

	.global asm_SineDots
asm_SineDots:
	.func 	asm_SineDots
	movem.l	d0-d7/a0-a3, -(a7)
	
	move.l	52(a7), a0	| frame
	lea		sin_x, a1
	lea		sin_y, a2
	move.l	56(a7), a3	| waves
	
	moveq	#0, d0
	moveq	#0, d1
	moveq	#0, d2
	moveq	#0, d3
	move.w	(a3), d0
	move.w	6(a3), d1
	move.w	12(a3), d2
	move.w	18(a3), d3
	
	add.w	2(a3), d0
	add.w	8(a3), d1
	add.w	14(a3), d2
	add.w	20(a3), d3
	move.w	d0, (a3)  
	move.w	d1, 6(a3) 
	move.w	d2, 12(a3)
	move.w	d3, 18(a3)
	
	move.w	#449, d7
1:
	move.w	(a1, d0.l), d4
	add.w	(a1, d1.l), d4
	move.w	(a2, d2.l), d5
	add.w	(a2, d3.l), d5
	move.w	d4, d6
	and.w	#0xf, d6
	sub.w	d6, d4
	lsr.w	#1, d4
	add.w	d4, d5
	move.w	#0x8000, d4
	lsr.w	d6, d4
	or.w	d4, (a0, d5.w)
	
	add.w	4(a3), d0
	add.w	10(a3), d1
	add.w	16(a3), d2
	add.w	22(a3), d3
	dbra	d7, 1b
	
	movem.l	(a7)+, d0-d7/a0-a3
	rts
	.endfunc


vbl:
	st		vblflag
	rte

	.bss
	.lcomm 	vblflag,	1
	.even
	.lcomm 	vblVec,		4
	.lcomm	sin_x,		65536
	.lcomm	sin_y,		65536
