#include <traps.h>

struct wave
{
	unsigned short pos;
	unsigned short speed;
	unsigned short offset;
};

void MakeSinusList(void);

#ifdef __cplusplus
extern "C" {
#endif
void asm_Init(void);
void asm_Exit(void);
int asm_WaitVbl(void);	// Returns false if space key pressed
void asm_FastClearScreen(void* frame);
void asm_FastClearScreen_1plane(void* frame);
void asm_SineDots(void* frame, struct wave waves[4]);
#ifdef __cplusplus
}
#endif

unsigned short screenBuf[16000 + 16000 + 128];
unsigned short* scr[2];

short * const palette = (short * const)0xffff8240;
unsigned char * const scrmode = (unsigned char * const)0xffff8260;
unsigned char * const vidbase = (unsigned char * const)0xffff8201;

short oldcol[2];
unsigned char oldscr;
unsigned char oldvid[2];

struct wave waves[4] = {
	{0, 100, 150},
	{0, 65500, 1034},
	{0, 120, 614},
	{0, 130, 174}
};

void Init(void)
{
	oldscr = *scrmode;
	oldvid[0] = vidbase[0];
	oldvid[1] = vidbase[2];
	oldcol[0] = palette[0];
	oldcol[1] = palette[1];
	*scrmode = 0;			// Low resolution
	palette[0] = 0x000;		// Black background
	palette[1] = 0xfff;		// White for bitplane 1
	
	unsigned int screenAdd = (unsigned int)&screenBuf;
	screenAdd = (screenAdd + 255) & ~255;	// Make it to an even 256 byte address.
	scr[0] = (unsigned short*)screenAdd;
	scr[1] = (unsigned short*)(screenAdd + 32000);

	MakeSinusList();

	asm_Init();
}

void Exit(void)
{
	palette[0] = oldcol[0];
	palette[1] = oldcol[1];
	vidbase[0] = oldvid[0];
	vidbase[2] = oldvid[1];
	*scrmode = oldscr;

	asm_Exit();
}

void Loop(void)
{
//	palette[0] = 0x020;
	
	// Flip screen
	unsigned short* frame = scr[1];
	scr[1] = scr[0];
	scr[0] = frame;
	vidbase[2] = (unsigned char)(((unsigned int)scr[0]) >> 8);
	vidbase[0] = (unsigned char)(((unsigned int)scr[0]) >> 16);
	
	asm_FastClearScreen_1plane(frame);
	asm_SineDots(frame, waves);
	
//	palette[0] = 0x000;
}

int SuperMain(void)
{
	Init();
	do
	{
		Loop();
	} while (asm_WaitVbl() == 0);
	Exit();

	return 0;
}

int main(int argc, char** argv)
{
	return Supexec(SuperMain);
}