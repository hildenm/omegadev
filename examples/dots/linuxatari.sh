#!/bin/bash

# Set this to where your Hatari ST hard disk is.
export ST_HD_PATH=/media/mikael/B00B-5000

# Set this to where your toolchain is. Most likely, just leave it as is.
export TOOLCHAIN_M68K=/usr/local/bin/m68k-elf

export GDB_COM_PORT=/dev/ttyACM0

# Start VsCode
code .

